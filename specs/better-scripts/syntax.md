# Syntax

This document describes the syntax of the BetterScripts language. Note that a syntaxically valid
script may be invalid for other reasons, for example because of duplicate action parameters.

## Notation

| Notation | Meaning |
|:--------:|:-------:|
| CAPITALS | A lexical token |
| *Italics* | An internal production rule |
| **_Bold_** | A top-level production rule |
| `string` | An exact string of characters |
| \n, \x13` | An escaped character |
| [`a`-`z`] | Any character in the range |
| [`e`, `E`] | Any character in the list |
| \~`e` | Any character except this one |
| x? | x, or nothing |
| x\* | Zero or more repetitions of x |
| x+ | One or more repetitions of x |
| *List*\[*Item*, *Sep*] | A list of elements; equivalent to:<br>(*Item* (*Sep* *Item*)\*)? |

## Lexical rules

A script is a sequence of UTF-8 codepoints, which are grouped into tokens as follows:

### Whitespace

WHITE tokens represent whitespace and comments. They are used to separate
other tokens, and are ignored by the grammar rules.

>**WHITE**:\
>| [\t, \n, \r, \s<sup>†</sup>]\
>| *SingleLineComment*\
>| *MultiLineComment*\
><sub>† ASCII space (U+0032) only</sub>
>
>*SingleLineComment*:\
>| `//` ~\n* \n<sup>†</sup>\
><sub>† Optional at the end of the input</sub>
>
>*MultiLineComment*:\
>| `/*` (~`*` | `*` ~`/`)* `*/`

### Identifiers

IDENT tokens represent the names of actions, functions and variables.

The following identifiers are reserved as keywords and are excluded from IDENT:
- `true`
- `false`

>**IDENT**:\
>| *IdentStart* *IdentContinue*\*
>
>*IdentStart*:\
>| [`a`-`z`, `A`-`Z`, `_`]
>
>*IdentContinue*:\
>| *IdentStart*\
>| [`0`-`9`]

### Numbers

NUMBER tokens represent both integers and floating point numbers.

>**NUMBER**:\
>| *NumMantissa* *NumExponent*?
>
>*NumMantissa*:\
>| *NumDigit*+\
>| `.` *NumDigit*+\
>| *NumDigit*+ `.` *NumDigit*\*
>
>*NumExponent*:\
>| [`e`, `E`] [`-`, `+`]? *NumDigit*+
>
>*NumDigit*:\
>| [`0`-`9`]

Additionally, numbers may not be immediately followed by a `.` or an *IdentStart* character.

### Strings

STRING tokens represent string literals, which can be either single- or double-quoted.

>**STRING**:\
>| `'` (\~[\x00-\0x31, `'`, `\`] | *EscapeSequence*)* `'`\
>| `"` (\~[\x00-\0x31, `"`, `\`] | *EscapeSequence*)* `"`
>
>*EscapeSequence*:\
>| `\b` | `\f` | `\n` | `\r` | `\t`\
>| `\'` | `\"` | `\\`

Other escapes sequences are currently allowed, but may become invalid in a future version.

### Punctuation

Any other character is treated as punctuation and may be matched as-is by the grammar rules.

## Grammar rules

Any two tokens may be separated by an arbitrary number of WHITE tokens.

>**_Script_**:\
>| *Statement*\*
>
>*Statement*:\
>| `(` *Expr* `)` `;`\
>| *ExprPlace* *AssignOp* *Expr* `;`\
>| *Action*
>
>*Action*:\
>| IDENT (`(` *ActionParamList* `)`)? *ActionModifier*\* (`;` | *ActionBlock*)
>
>*ActionParamList*<sup>†</sup>:\
>| *Expr*\
>| *List*\[*ActionParam*, `,`]\
><sub>† *A single* IDENT *is always parsed as an Expr.*</sub>
>
>*ActionParam*:\
>| IDENT (`=` *Expr*)?
>
>*ActionModifier*:\
>| IDENT (`=` *ExprLit*)?
>
>*ActionBlock*:\
>| `{` *Statement*\* `}`
>
>*Expr*<sup>†</sup>:\
>| `(` *Expr* `)`\
>| *ExprLit*\
>| *ExprCall*\
>| *ExprPlace*\
>| *PrefixOp* *Expr*\
>| *Expr* *InfixOp* *Expr*\
><sub>† *If multiple rules match, the first one wins.*</sub>
>
>*ExprPlace*:\
>| IDENT *VarAccess*\*
>
>*VarAccess*:\
>| `.` IDENT\
>| `[` *Expr* `]`
>
>*ExprCall*:\
>| IDENT `(` *List*\[*Expr*, `,`] `)`
>
>*ExprLit*:\
>| *ExprConst*\
>| `-`? NUMBER\
>| STRING\
>| `true`\
>| `false`
>
>*ExprConst*<sup>†</sup>:\
>| `#` *ExprCall*\
>| `#` *ExprPlace*\
>| `#(` *Expr* `)`\
><sub>† *If multiple rules match, the first one wins. Cannot contain nested ExprConst productions.*</sub>
>
>*InfixOp*<sup>†</sup>:\
>| `*` | `/` | `%`\
>| `+` | `-`\
>| `>` | `<` | `>=` | `<=` | `==` | `!=`\
>| `&` | `|` | `^`\
>| `&&`\
>| `||`\
><sub>† *Ordered by decreasing precedence; always left-associative.*</sub>
>
>*PrefixOp*:\
>| `-` | `!` | `~`
>
>*AssignOp*:\
>| `=` | `+=` | `-=` |`/=` |`*=` | `%=` | `|=` | `&=` | `^=`
