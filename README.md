# Eternalfest Compiler and Tools

This monorepo contains the Eternalfest Compiler (`efc`), and related tools.

## Projects list
- `@eternalfest/better-scripts`: [./packages/better-scripts/](./packages/better-scripts/)
- `@eternalfest/efc`: [./packages/efc/](./packages/efc/)
