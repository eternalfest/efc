import * as assert from "node:assert/strict";
import fs from "node:fs";
import path from "node:path";
import { describe, test } from "node:test";

import { ConstValue, EvalContext, VarResolver } from "../lib/const-eval.mjs";
import { ErrorSink } from "../lib/error.mjs";
import { compileScript } from "../lib/index.mjs";
import { dirname } from "./meta.mjs";

const SCRIPT_EXT: string = ".script";
const JSON_EXT: string = ".json";
const SCRIPT_DIR: string = path.resolve(dirname, "test-resources/script-data");

function getScriptsInDir(dirname: string): string[] {
  return fs.readdirSync(dirname, {withFileTypes: true})
    .filter(f => f.isFile() && f.name.endsWith(SCRIPT_EXT))
    .map(f => f.name.substring(0, f.name.length - SCRIPT_EXT.length));
}

const CONST_EVAL_RESOLVER: VarResolver = name => {
  if (name === "echo") {
    return {
      type: "object",
      getField(cx: EvalContext, f: string): ConstValue {
        return cx.string(name + "." + f);
      },
      call(cx: EvalContext, args: ConstValue[]): ConstValue {
        const nums = args.map(a => cx.expect(a, "number"));
        return cx.string(name + "(" + nums.join(", ") + ")");
      }
    };
  }

  if (name === "empty") {
    return {type: "object"};
  }

  return undefined;
};

describe("parse", () => {
  for (const name of getScriptsInDir(SCRIPT_DIR)) {
    const input: string = fs.readFileSync(path.resolve(SCRIPT_DIR, name + SCRIPT_EXT)).toString();
    const expected: any = JSON.parse(fs.readFileSync(path.resolve(SCRIPT_DIR, name + JSON_EXT)).toString());
    const expectedErrors = expected.errors;
    const expectedScript = expected.script;
    const hasErrors = expectedErrors.length > 0;

    test(`${hasErrors ? "errors on" : "parses the"} script '${name}'`, () => {
      const actualErrors: string[] = [];
      const errorSink: ErrorSink = e => actualErrors.push(e.msg);

      const actualScript = compileScript(input, errorSink, CONST_EVAL_RESOLVER);

      assert.deepStrictEqual(actualErrors, expectedErrors);
      assert.deepStrictEqual(actualScript, expectedScript);
    });
  }
});
