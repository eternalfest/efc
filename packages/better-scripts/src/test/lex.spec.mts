import * as assert from "node:assert/strict";
import { describe, test } from "node:test";

import { Lexer, SpannedTok, Token, unescapeString } from "../lib/lex.mjs";
import { Span } from "../lib/text.mjs";

interface TestCase {
  contents: string;
  tokens: [Token, number, number][];
}

const TEST_CASES: TestCase[] = [
  {
    contents: "",
    tokens: [],
  },
  {
    contents: "foo bar baz quux true false",
    tokens: [
      [Token.Ident, 0, 3], [Token.Ident, 4, 7], [Token.Ident, 8, 11],
      [Token.Ident, 12, 16], [Token.LitBoolTrue, 17, 21], [Token.LitBoolFalse, 22, 27],
    ],
  },
  {
    contents: "\"foo\" '\"bâr\"' '\\\\nbaz\\t' ''",
    tokens: [[Token.LitString, 0, 5], [Token.LitString, 6, 13], [Token.LitString, 14, 24], [Token.LitString, 25, 27]],
  },
  {
    contents: "00 1  5432  42 1.0  .2 12e34 0.123e-01  999e+999",
    tokens: [
      [Token.LitNumber, 0, 2], [Token.LitNumber, 3, 4], [Token.LitNumber, 6, 10],
      [Token.LitNumber, 12, 14], [Token.LitNumber, 15, 18], [Token.LitNumber, 20, 22],
      [Token.LitNumber, 23, 28], [Token.LitNumber, 29, 38], [Token.LitNumber, 40, 48],
    ],
  },
  {
    contents: "( )[]{ }  ,; === +=-=/=*=%=|=&=^= !=>=<=>< &&||! +-*/%&|^~ .##(",
    tokens: [
      [Token.LParen, 0, 1], [Token.RParen, 2, 3], [Token.LBracket, 3, 4],
      [Token.RBracket, 4, 5], [Token.LBrace, 5, 6], [Token.RBrace, 7, 8],
      [Token.Comma, 10, 11], [Token.Semicolon, 11, 12], [Token.Equals, 13, 15],
      [Token.Assign, 15, 16], [Token.AddAssign, 17, 19], [Token.SubAssign, 19, 21],
      [Token.DivAssign, 21, 23], [Token.MulAssign, 23, 25], [Token.ModAssign, 25, 27],
      [Token.OrAssign, 27, 29], [Token.AndAssign, 29, 31], [Token.XorAssign, 31, 33],
      [Token.NotEquals, 34, 36], [Token.GreaterThanEquals, 36, 38], [Token.LesserThanEquals, 38, 40],
      [Token.GreaterThan, 40, 41], [Token.LesserThan, 41, 42], [Token.LogicalAnd, 43, 45],
      [Token.LogicalOr, 45, 47], [Token.LogicalNot, 47, 48], [Token.Add, 49, 50],
      [Token.Sub, 50, 51], [Token.Mul, 51, 52], [Token.Div, 52, 53],
      [Token.Mod, 53, 54], [Token.BitwiseAnd, 54, 55], [Token.BitwiseOr, 55, 56],
      [Token.BitwiseXor, 56, 57], [Token.BitwiseNot, 57, 58], [Token.Dot, 59, 60],
      [Token.Hash, 60, 61], [Token.HashLParen, 61, 63],
    ],
  },
  {
    contents: `
      foo
      //comment /*
      bar
      /* comment //*/
      baz/**/quux
    `,
    tokens: [[Token.Ident, 7, 10], [Token.Ident, 36, 39], [Token.Ident, 68, 71], [Token.Ident, 75, 79]],
  },
];

const INVALID_TEST_CASES: string[] = [
  "invalid char £",
  "/* unterminated comment",
  "'unterminated string",
  "'\t string with control char'",
  "malformed number .1.2",
  "malformed number 1..2",
  "malformed number 0.e12",
  "malformed number 12ee3",
  "malformed number 1.2z",
  "malformed number 1.2e",
];

const STRING_UNESCAPE_CASES: string[][] = [
  ["foo", "foo"],
  ["\\'", "'"],
  ["\\b\\f a\\\\a\\n \\r\\t", "\b\f a\\a\n \r\t"],
];

function makeToken([kind, start, end]: [Token, number, number]): SpannedTok {
  const span = Span.of(start, end);
  return {kind, span};
}

describe("lex", () => {
  describe("string unescaping", () => {
    for (const [escaped, expected] of STRING_UNESCAPE_CASES) {
      test(`unescape string: ${escaped}`, () => assert.strictEqual(expected, unescapeString(escaped)));
    }
  });

  test("next(), peek() and pos() work correctly", () => {
    const lexer = new Lexer("{ [ (");
    assert.strictEqual(0, lexer.pos());
    assert.strictEqual(Token.LBrace, lexer.peek().kind);
    assert.strictEqual(Token.LBrace, lexer.next().kind);
    assert.strictEqual(1, lexer.pos());
    assert.strictEqual(Token.LBracket, lexer.next().kind);
    assert.strictEqual(3, lexer.pos());
    assert.strictEqual(Token.LParen, lexer.peek().kind);
    assert.strictEqual(3, lexer.pos());
    assert.strictEqual(Token.LParen, lexer.peek().kind);
    assert.strictEqual(Token.LParen, lexer.next().kind);
    assert.strictEqual(5, lexer.pos());
    assert.strictEqual(Token.EOF, lexer.next().kind);
    assert.strictEqual(Token.EOF, lexer.next().kind);
    assert.strictEqual(5, lexer.pos());
  });

  describe("test data", () => {
    for (const {contents, tokens} of TEST_CASES) {
      const lexer = new Lexer(contents);
      test(`lexes the string: ${contents}`, () => {
        for (const tok of tokens!) {
          assert.deepStrictEqual(makeToken(tok), lexer.next(), "unexpected token");
        }
        const eof = makeToken([Token.EOF, contents.length, contents.length]);
        assert.deepStrictEqual(eof, lexer.next(), "unexpected token");
      });
    }

    for (const contents of INVALID_TEST_CASES) {
      const lexer = new Lexer(contents);
      test(`errors on invalid string: ${contents}`, () => {
        assert.throws(() => {
          while (lexer.next().kind !== Token.EOF) {
            // consume the token
          }
        });
      });
    }
  });
});
