// TODO: Add documentation

import * as ast from "./ast.mjs";
import { ErrorKind, Result, ScriptError } from "./error.mjs";
import { Span } from "./text.mjs";

export type VarResolver = (name: string) => ConstValue | undefined;

export type ConstPrimitive = ConstNumber | ConstString | ConstBool | ConstNull;
export type ConstValue = ConstNumber | ConstString | ConstBool | ConstNull | ConstObject;

export interface ConstNumber {
  readonly type: "number";
  readonly value: number;
}

export interface ConstString {
  readonly type: "string";
  readonly value: string;
}

export interface ConstBool {
  readonly type: "boolean";
  readonly value: boolean;
}

export interface ConstNull {
  readonly type: "null";
  readonly value: null;
}

export interface ConstObject {
  readonly type: "object";
  getField?(cx: EvalContext, name: string): ConstValue | undefined;
  call?(cx: EvalContext, args: ConstValue[]): ConstValue;
}

// Helper types for EvalContext.expect methods
type ConstTypePrim = "number" | "string" | "boolean" | "null";
type ConstTypeOf<T extends ConstTypePrim> =
  T extends "number" ? number :
  T extends "string" ? string :
  T extends "boolean" ? boolean :
  T extends "null" ? null : unknown;
type FixedSizeArray<N extends number, T> = N extends 0 ? never[] : {
  0: T;
  length: N;
} & ReadonlyArray<T>;

const CONST_NULL: ConstNull = { type: "null", value: null };
const CONST_TRUE: ConstBool = { type: "boolean", value: true };
const CONST_FALSE: ConstBool = { type: "boolean", value: false };

export class EvalContext {
  #resolveInner: VarResolver;
  #curSpan: Span;

  public static eval(expr: ast.Expr, resolve: VarResolver): Result<ConstValue, ErrorKind.ConstEval> {
    const cx = new EvalContext(resolve, expr.span);
    try {
      return cx.#eval(expr);
    } catch (e) {
      if (e instanceof ConstErrorCarrier) {
        return e.error;
      }
      throw e;
    }
  }

  private constructor(resolve: VarResolver, span: Span) {
    this.#resolveInner = resolve;
    this.#curSpan = span;
  }

  public number(value: number): ConstNumber {
    // Normalize -0.0 as +0.0 because Flash is weird and doesn't have negative zero.
    return { type: "number", value: value + 0 };
  }

  public string(value: string): ConstString {
    return { type: "string", value };
  }

  public boolean(value: boolean): ConstBool {
    return value ? CONST_TRUE : CONST_FALSE;
  }

  public null(): ConstNull {
    return CONST_NULL;
  }

  #error(msg: string, span: Span): never {
    throw new ConstErrorCarrier({
      type: "error",
      kind: ErrorKind.ConstEval,
      msg: "Const eval: " + msg,
      spans: [span],
    });
  }

  public error(msg: string): never {
    this.#error(msg, this.#curSpan);
  }

  #expect<T extends ConstTypePrim>(val: ConstValue, expected: T, e: ast.Expr): ConstTypeOf<T> {
    if (val.type !== expected) {
      this.#error(`expected ${expected}, got ${val.type}`, e.span);
    }
    return val.value as any;
  }

  public expect<T extends ConstTypePrim>(val: ConstValue, expected: T): ConstTypeOf<T> {
    if (val.type !== expected) {
      this.#error(`expected ${expected}, got ${val.type}`, this.#curSpan);
    }
    return val.value as any;
  }

  public expectArgs<N extends number>(args: ConstValue[], count: N): FixedSizeArray<N, ConstValue> {
    if (args.length !== count) {
      this.#error(`expected ${count} arguments, got ${args.length}`, this.#curSpan);
    }
    return args as any;
  }

  #resolve({ span, ident }: ast.Ident): ConstValue {
    this.#curSpan = span;
    const val = this.#resolveInner(ident);
    if (val === undefined) {
      this.error("unknown variable " + ident);
    }
    return val;
  }

  #eval(expr: ast.Expr): ConstValue {
    switch (expr.type) {
      case "const":
        return this.#eval(expr.expr);
      case "lit": {
        const value = expr.value;
        switch (typeof value) {
          case "number": return this.number(value);
          case "string": return this.string(value);
          case "boolean": return this.boolean(value);
          case "object": return this.null();
          default:
            throw new Error("unexpectedLitKind: " + typeof value);
        }
      }
      case "var": {
        let cur = this.#resolve(expr.name);
        for (const part of expr.parts) {
          const field = this.#evalFieldName(part);
          this.#curSpan = part.span;
          if (cur.type === "object" && cur.getField !== undefined) {
            cur = cur.getField(this, field) ?? this.error("unknown field " + field);
          } else {
            this.error(cur.type + " value doesn't have fields");
          }
        }
        return cur;
      }
      case "call": {
        const fn = this.#resolve(expr.func);
        const args = expr.args.map(e => this.#eval(e));
        if (fn.type === "object" && fn.call !== undefined) {
          this.#curSpan = expr.func.span;
          return fn.call(this, args);
        } else {
          return this.error(fn.type + " value cannot be called as a function");
        }
      }
      case "unaryop":
        return this.#evalUnaryOp(expr);
      case "binop": {
        return this.#evalBinOp(expr);
      }
    }
  }

  #evalFieldName(field: ast.VarField | ast.Expr): string {
    if (field.type === "field") {
      return field.name;
    } else {
      const val = this.#eval(field);
      switch (val.type) {
        case "string": return val.value;
        // TODO: support integer fields: what are the exact semantics?
        default: this.#error(val.type + " values cannot be used as fields", field.span);
      }
    }
  }

  #evalUnaryOp(e: ast.ExprUnaryOp): ConstValue {
    switch (e.op) {
      case "!": {
        const v = this.#expect(this.#eval(e.expr), "boolean", e);
        return this.boolean(!v);
      }
      case "~": {
        const v: number = this.#expect(this.#eval(e.expr), "number", e);
        return this.number(~v);
      }
      case "-": {
        const v: number = this.#expect(this.#eval(e.expr), "number", e);
        return this.number(-v);
      }
      default: this.#error(`unsupported unary operator '${e.op}'`, e.span);
    }
  }

  #evalBinOp(e: ast.ExprBinOp): ConstValue {
    switch (e.op) {
      case "==": return this.boolean(this.#evalEquals(e.lhs, e.rhs));
      case "!=": return this.boolean(!this.#evalEquals(e.lhs, e.rhs));
    }

    const l = this.#eval(e.lhs);
    const r = this.#eval(e.rhs);

    switch (e.op) {
      case "+":
        switch (l.type) {
          case "number": return this.number(l.value + this.#expect(r, "number", e.rhs));
          case "string": return this.string(l.value + this.#expect(r, "string", e.rhs));
          default: return this.#error("expected number or string, got " + l.type, e.lhs.span);
        }
      // Note: no short-circuit.
      case "&&": return this.boolean(
        this.#expect(l, "boolean", e.lhs) && this.#expect(r, "boolean", e.rhs)
      );
      // Note: no short-circuit.
      case "||": return this.boolean(
        this.#expect(l, "boolean", e.lhs) || this.#expect(r, "boolean", e.rhs)
      );
    }

    // All other binops operate exclusively on numbers.
    const lnum = this.#expect(l, "number", e.lhs);
    const rnum = this.#expect(r, "number", e.rhs);
    switch (e.op) {
      case "-": return this.number(lnum - rnum);
      case "*": return this.number(lnum * rnum);
      case "/": return this.number(lnum / rnum);
      case "%": return this.number(lnum % rnum);
      case "^": return this.number(lnum ^ rnum);
      case "|": return this.number(lnum | rnum);
      case "&": return this.number(lnum & rnum);
    }

    // Disallow comparisons with NaNs: they behave weirdly in Flash.
    if (isNaN(lnum)) {
      this.#error("cannot compare NaN values", e.lhs.span);
    } else if (isNaN(rnum)) {
      this.#error("cannot compare NaN values", e.rhs.span);
    }

    switch (e.op) {
      case ">": return this.boolean(lnum > rnum);
      case ">=": return this.boolean(lnum >= rnum);
      case "<": return this.boolean(lnum < rnum);
      case "<=": return this.boolean(lnum <= rnum);
      default: this.#error(`unsupported binary operator '${e.op}'`, e.span);
    }
  }

  #evalEquals(lhs: ast.Expr, rhs: ast.Expr): boolean {
    const l = this.#eval(lhs);
    const r = this.#eval(rhs);

    if (l.type === "object") {
      this.#error("cannot compare objects for equality", lhs.span);
    } else if (r.type === "object") {
      this.#error("cannot compare objects for equality", rhs.span);
    } else if (l.type !== r.type) {
      this.#error(`cannot compare values of different types (${l.type}, ${r.type})`, rhs.span);
    } else if (Object.is(l.value, NaN) && Object.is(r.value, NaN)) {
      // Disallow comparisons of two NaNs: they behave weirdly in Flash.
      this.#error("cannot compare NaN values", lhs.span);
    }

    return l.value === r.value;
  }
}

class ConstErrorCarrier {
  error: ScriptError<ErrorKind.ConstEval>;

  constructor(err: ScriptError<ErrorKind.ConstEval>) {
    this.error = err;
  }
}
