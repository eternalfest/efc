import { Span } from "./text.mjs";

export type Result<T, K extends ErrorKind = ErrorKind> = T | ScriptError<K>;

export type ErrorSink = (err: ScriptError) => void;

export enum ErrorKind {
  Lexical,
  Syntaxic,
  Semantic,
  ConstEval,
}

export interface ScriptError<K extends ErrorKind = ErrorKind> {
  readonly type: "error";
  readonly kind: K;
  readonly msg: string;
  readonly spans: ReadonlyArray<Span>;
}

