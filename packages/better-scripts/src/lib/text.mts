
// Represent a span in the source string.
export class Span {
  public readonly start: number;
  public readonly end: number;

  // @ts-ignore allow unused variable
  private __noDuckTyping: undefined;

  constructor(start: number, end: number) {
    if (start < 0 || Math.floor(start) !== start || start > Number.MAX_SAFE_INTEGER) {
      throw new TypeError(`start must be a non-negative integer (is ${start}`);
    } else if (end < 0 || Math.floor(end) !== end || end > Number.MAX_SAFE_INTEGER) {
      throw new TypeError(`end must be a non-negative integer (is ${end}`);
    } else if (start > end) {
      throw new TypeError(`start cannot be greater than end (${start} > ${end})`);
    }
    this.start = start;
    this.end = end;
  }

  public static of(start: number, end: number): Span {
    return new Span(start, end);
  }

  public static ofText(text: string): Span {
    return new Span(0, text.length);
  }

  public static readonly DUMMY: Span = new Span(0, 0);
}

// An object with a span.
export interface Spanned {
  span: Span;
}
