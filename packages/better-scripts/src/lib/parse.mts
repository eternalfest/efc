import * as ast from "./ast.mjs";
import { ErrorKind, Result } from "./error.mjs";
import { getTokenName, Lexer, LexError, SpannedTok, Token, TokenFamily, unescapeString } from "./lex.mjs";
import * as lex from "./lex.mjs";
import { Span } from "./text.mjs";

type ParseResult<T> = Result<T, ErrorKind.Lexical | ErrorKind.Syntaxic>;

export function parseScript(text: string): ParseResult<ast.Script> {
  try {
    const ast: ast.Script = {
      type: "script",
      span: Span.ofText(text),
      nodes: [],
    };
    const ctx = new Context(text);
    while (ctx.matches(Token.EOF) === undefined) {
      parseNodeInto(ctx, ast.nodes);
    }
    return ast;
  } catch (e) {
    return handleCaughtError(e);
  }
}

export function parseExprConst(text: string): ParseResult<ast.ExprConst> {
  try {
    const ctx = new Context(text);
    const ast = parseConstExpr(ctx, ctx.consumeFamily(lex.TOKEN_CONST));
    // Can't use `consume(Token.EOF)` as it would eat whitespace, which we don't want.
    if (ast.span.end < text.length) {
      throw new LexError("Unexpected trailing input", Span.of(ast.span.end, text.length));
    }
    return ast;
  } catch (e) {
    return handleCaughtError(e);
  }
}

function handleCaughtError(err: any): ParseResult<never> {
  if (err instanceof LexError) {
    return { type: "error", kind: ErrorKind.Lexical, msg: err.msg, spans: [err.span] };
  } else if (err instanceof ParseError) {
    return { type: "error", kind: ErrorKind.Syntaxic, msg: err.msg, spans: [err.span] };
  } else {
    throw err;
  }
}

function parseNodeInto(ctx: Context, nodes: ast.Node[]): void {
  const tok = ctx.consume2(Token.Ident, Token.LParen);

  // Expression statements
  if (tok.kind === Token.LParen) {
    const expr = parseExpression(ctx);
    ctx.consume(Token.RParen);
    ctx.consume(Token.Semicolon);
    expr.span = ctx.spanFrom(tok.span);
    nodes.push(expr);
    return;
  }

  const nameOrVar = parseExprVariable(ctx, ctx.getIdent(tok.span));

  const assign = ctx.tryConsumeFamily(lex.TOKENS_ASSIGN);
  // Action statements
  if (assign === undefined) {
    if (nameOrVar.parts.length > 0) {
      return ctx.unexpected();
    }

    const action: ast.Action = {
      type: "action",
      span: nameOrVar.span,
      name: nameOrVar.name,
      params: [],
      children: [],
      modifiers: [],
    };

    try {
      parseActionContents(ctx, action);
    } finally {
      action.span = ctx.spanFrom(action.span);
      nodes.push(action);
    }

  // Assignment statements
  } else {
    const op = assign.kind === Token.Assign ? null : getTokenAssignOpName(assign.kind);
    const rhs = parseExpression(ctx);
    ctx.tryConsume(Token.Semicolon);

    const span = ctx.spanFrom(nameOrVar.span);
    nodes.push({ type: "assign", span, lhs: nameOrVar, rhs, op});
  }
}

function parseActionContents(ctx: Context, action: ast.Action): void {
  // Parameter list
  if (ctx.tryConsume(Token.LParen) !== undefined) {
    if (ctx.tryConsume(Token.RParen) === undefined) {
      let preferAnonParam = true;
      do {
        action.params.push(parseParameter(ctx, preferAnonParam));
        preferAnonParam = false;
      } while (ctx.consume2(Token.Comma, Token.RParen).kind === Token.Comma);
    }
  }

  // Modifiers
  while (ctx.matches(Token.Ident) !== undefined) {
    action.modifiers.push(parseParameter(ctx, null));
  }

  // Child nodes
  const tok = ctx.consume2(Token.LBrace, Token.Semicolon);
  if (tok.kind === Token.LBrace) {
    while (ctx.tryConsume(Token.RBrace) === undefined) {
      parseNodeInto(ctx, action.children);
    }
  }
}

// preferAnonParam controls how ambiguities between named
// and anonymous parameters are resolved:
//   - null: anonymous parameters are a syntax error
//   - false: 'foo' is parsed as a named param. set to 'true'
//   - true: 'foo' (and only if last) is parsed as an anonymous param.
function parseParameter(ctx: Context, preferAnonParam: null): ast.NamedParam;
function parseParameter(ctx: Context, preferAnonParam: boolean): ast.ActionParam;
function parseParameter(ctx: Context, preferAnonParam: boolean | null): ast.ActionParam {
  let name: ast.Ident;
  let expr: ast.Expr;
  if (preferAnonParam === null) {
    name = ctx.getIdent(ctx.consume(Token.Ident).span);
  } else {
    expr = parseExpression(ctx);
    if (expr.type !== "var" || expr.parts.length > 0) {
      // Must be a complex expression, return it as anon. param
      return { name: null, value: expr };
    }

    name = expr.name;
  }

  if (ctx.tryConsume(Token.Assign) === undefined) {
    if (preferAnonParam && ctx.matches(Token.RParen) !== undefined) {
      return { name: null, value: expr! };
    } else {
      return { name, value: { type: "lit", span: name.span, value: true } };
    }
  }

  return { name, value: parseExpression(ctx) };
}

function parseExpression(ctx: Context, precedence: number = 0): ast.Expr {
  // Infix operators: this is the precedence climbing algorithm
  let lhs = parseExprAtom(ctx);

  for (;;) {
    const infix = ctx.matchesFamily(lex.TOKENS_INFIX);
    if (infix === undefined) {
      break;
    }
    const curPrec = lex.TOKENS_INFIX.tokens.get(infix)!;
    if (curPrec < precedence) {
      break;
    }
    ctx.next();
    const op = getTokenOpName(infix);

    // All infix op. are left-associative
    const rhs = parseExpression(ctx, curPrec + 1);

    const span = ctx.spanFrom(lhs.span);
    lhs = { type: "binop", span, lhs, rhs, op };
  }
  return lhs;
}

function parseExprAtom(ctx: Context): ast.Expr {
  // Parenthesized expressions
  const parenTok = ctx.tryConsume(Token.LParen);
  if (parenTok !== undefined) {
    const expr = parseExpression(ctx);
    ctx.consume(Token.RParen);
    expr.span = ctx.spanFrom(parenTok.span);
    return expr;
  }

  // Prefix operators
  const prefixTok = ctx.tryConsumeFamily(lex.TOKENS_PREFIX);
  if (prefixTok !== undefined) {
    const expr = parseExprAtom(ctx);
    const span = ctx.spanFrom(prefixTok.span);

    // Special case for negative numbers
    if (prefixTok.kind === Token.Sub && expr.type === "lit" && typeof expr.value === "number") {
      return { type: "lit", span, value: -expr.value };
    } else {
      const op = getTokenOpName(prefixTok.kind);
      return { type: "unaryop", span, op, expr };
    }
  }

  // Variables and function calls
  const nameTok = ctx.tryConsume(Token.Ident);
  if (nameTok !== undefined) {
    const name = ctx.getIdent(nameTok.span);
    return ctx.matches(Token.LParen) === undefined ? parseExprVariable(ctx, name) : parseExprCall(ctx, name);
  }

  // Const expressions
  const constTok = ctx.tryConsumeFamily(lex.TOKEN_CONST);
  if (constTok !== undefined) {
    return parseConstExpr(ctx, constTok);
  }

  // Literals
  return parseLiteral(ctx);
}

function parseConstExpr(ctx: Context, constTok: SpannedTok): ast.ExprConst {
  let expr: ast.Expr;
  switch (constTok.kind) {
    case Token.Hash: {
      // Variables and function calls
      const nameTok = ctx.consume(Token.Ident);
      const name = ctx.getIdent(nameTok.span);
      expr = ctx.matches(Token.LParen) === undefined ? parseExprVariable(ctx, name) : parseExprCall(ctx, name);
      break;
    }
    case Token.HashLParen:
      // Arbitrary expressions.
      expr = parseExpression(ctx);
      ctx.consume(Token.RParen);
      break;
    default:
      throw new Error(`Unreachable: unexpected token type: ${getTokenName(constTok.kind)}`);
  }

  const span = ctx.spanFrom(constTok.span);
  return { type: "const", span, expr };
}

function parseExprVariable(ctx: Context, name: ast.Ident): ast.ExprVar {
  const parts: (ast.VarField | ast.Expr)[] = [];

  for (;;) {
    if (ctx.tryConsume(Token.Dot) !== undefined) {
      const field = ctx.getIdent(ctx.consume(Token.Ident).span);
      parts.push({
        type: "field",
        name: field.ident,
        span: field.span,
      });
    } else if (ctx.tryConsume(Token.LBracket) !== undefined) {
      parts.push(parseExpression(ctx));
      ctx.consume(Token.RBracket);
    } else {
      const span = ctx.spanFrom(name.span);
      return { type: "var", span, name, parts };
    }
  }
}

function parseExprCall(ctx: Context, func: ast.Ident): ast.ExprCall {
  const args: ast.Expr[] = [];
  ctx.consume(Token.LParen);
  if (ctx.tryConsume(Token.RParen) === undefined) {
    do {
      args.push(parseExpression(ctx));
    } while (ctx.tryConsume(Token.Comma) !== undefined);
    ctx.consume(Token.RParen);
  }

  const span = ctx.spanFrom(func.span);
  return { type: "call", span, func, args };
}

function parseLiteral(ctx: Context): ast.ExprValue {
  const tok = ctx.consumeFamily(lex.TOKENS_LITERAL);
  switch (tok.kind) {
    case Token.LitBoolFalse:
      return { type: "lit", value: false, span: tok.span };
    case Token.LitBoolTrue:
      return { type: "lit", value: true, span: tok.span };
    case Token.LitNumber:
      return ctx.getLitNumber(tok.span);
    default:
      return ctx.getLitString(tok.span);
  }
}

function getTokenAssignOpName(op: Token): string {
  const opName = getTokenName(op);
  return opName.substring(1, opName.length - 2);
}

function getTokenOpName(op: Token): string {
  const opName = getTokenName(op);
  return opName.substring(1, opName.length - 1);
}

class Context {
  readonly script: string;
  private readonly lexer: Lexer;
  private readonly expectedKinds: string[];

  constructor(script: string) {
    this.script = script;
    this.lexer = new Lexer(script);
    this.expectedKinds = [];
  }

  curPos(): number {
    return this.lexer.curPos;
  }

  spanFrom(span: Span): Span {
    return Span.of(span.start, this.lexer.curPos);
  }

  next(): SpannedTok {
    this.expectedKinds.length = 0;
    return this.lexer.next();
  }

  matches(kind: Token): Token | undefined {
    this.expectedKinds.push(getTokenName(kind));
    const tok = this.lexer.peek().kind;
    return tok === kind ? tok : undefined;
  }

  matchesFamily<T>(family: TokenFamily<T>): Token | undefined {
    this.expectedKinds.push(family.name);
    const tok = this.lexer.peek().kind;
    return family.tokens.has(tok) ? tok : undefined;
  }

  consume(kind: Token): SpannedTok {
    const tok = this.tryConsume(kind);
    if (tok === undefined) {
      return this.unexpected();
    }
    return tok;
  }

  consume2(kind1: Token, kind2: Token): SpannedTok {
    let tok = this.tryConsume(kind1);
    if (tok === undefined) {
      tok = this.tryConsume(kind2);
      if (tok === undefined) {
        return this.unexpected();
      }
    }
    return tok;
  }

  consumeFamily<T>(family: TokenFamily<T>): SpannedTok {
    const tok = this.tryConsumeFamily(family);
    if (tok === undefined) {
      return this.unexpected();
    }
    return tok;
  }

  unexpected(): never {
    const tok = this.lexer.peek();
    const expected = new Set([...this.expectedKinds]);
    expected.delete(getTokenName(Token.EOF));
    if (expected.size === 0) {
      throw new ParseError(`Unexpected ${getTokenName(tok.kind)}`, tok.span);
    }

    const parts = ["Unexpected ", getTokenName(tok.kind), "; expected "];
    for (const kind of [...expected.values()].sort()) {
      parts.push(kind);
      parts.push(", ");
    }
    parts.pop();
    parts[parts.length - 2] = " or ";

    this.expectedKinds.length = 0;
    throw new ParseError(parts.join(""), tok.span);
  }

  tryConsume(kind: Token): SpannedTok | undefined {
    const tok = this.lexer.peek().kind;
    if (tok === kind) {
      this.expectedKinds.length = 0;
      return this.lexer.next();
    }
    this.expectedKinds.push(getTokenName(kind));
    return undefined;
  }

  tryConsumeFamily<T>(family: TokenFamily<T>): SpannedTok | undefined {
    const tok = this.lexer.peek().kind;
    if (family.tokens.has(tok)) {
      this.expectedKinds.length = 0;
      return this.lexer.next();
    }

    this.expectedKinds.push(family.name);
    return undefined;
  }

  getIdent(span: Span): ast.Ident {
    return {
      span,
      ident: this.script.substring(span.start, span.end),
    };
  }

  getLitNumber(span: Span): ast.ExprValue<number> {
    return {
      span,
      type: "lit",
      value: parseFloat(this.script.substring(span.start, span.end)),
    };
  }

  getLitString(span: Span): ast.ExprValue<string> {
    if (span.end - span.start <= 2) {
      return { span, type: "lit", value: "" };
    }

    const str = this.script.substring(span.start + 1, span.end - 1);
    return {
      span,
      type: "lit",
      value: unescapeString(str),
    };
  }
}

class ParseError {
  msg: string;
  span: Span;

  constructor(msg: string, span: Span) {
    this.msg = msg;
    this.span = span;
  }
}
