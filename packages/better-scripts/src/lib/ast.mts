import { Spanned } from "./text.mjs";

// A complete script.
export interface Script extends Spanned {
  type: "script";
  nodes: Node[];
}

// A script node.
export type Node = Action | VarAssign | Expr;

// A script action.
export interface Action extends Spanned {
  type: "action",
  name: Ident;
  children: Node[];
  params: ActionParam[];
  modifiers: NamedParam[];
}

// An action parameter.
export type ActionParam = NamedParam | AnonParam;

// A named parameter.
export interface NamedParam {
  name: Ident;
  value: Expr;
}

// An anonymous parameter.
export interface AnonParam {
  name: null;
  value: Expr;
}

// A variable assignment.
export interface VarAssign extends Spanned {
  type: "assign";
  lhs: ExprVar;
  op: string | null;
  rhs: Expr;
}

// A spanned identifier.
export interface Ident extends Spanned {
  ident: string;
}

// An expression.
export type Expr = ExprValue | ExprConst | ExprBinOp | ExprUnaryOp | ExprCall | ExprVar;

export type Value = boolean | number | string;

// A literal value.
export interface ExprValue<V extends Value = Value> extends Spanned {
  type: "lit";
  value: V;
}

// An expression evaluated in const context.
export interface ExprConst extends Spanned {
  type: "const";
  expr: Expr;
}

// An infix binary operation.
export interface ExprBinOp extends Spanned {
  type: "binop";
  op: string;
  lhs: Expr;
  rhs: Expr;
}

// A prefix unary operation.
export interface ExprUnaryOp extends Spanned {
  type: "unaryop";
  op: string;
  expr: Expr;
}

// A function call.
export interface ExprCall extends Spanned {
  type: "call";
  func: Ident;
  args: Expr[];
}

// A variable access.
export interface ExprVar extends Spanned {
  type: "var";
  name: Ident;
  parts: (VarField | Expr)[];
}

// A field name.
export interface VarField extends Spanned {
  type: "field",
  name: string,
}
