import * as ast from "./ast.mjs";
import { EvalContext, VarResolver } from "./const-eval.mjs";
import { ErrorKind, ErrorSink } from "./error.mjs";
import { Span } from "./text.mjs";

export type Mtbon = null | number | boolean | string | Mtbon[] | { [P: string]: Mtbon };

type CompiledNode = Record<string, Mtbon> & {
  name: string;
  params?: Mtbon;
  children?: CompiledNode[] | null;
};

class Context {
  readonly error: ErrorSink;
  readonly resolve: VarResolver;
  #ops: Mtbon[];

  constructor(error: ErrorSink, resolve: VarResolver) {
    this.error = error;
    this.resolve = resolve;
    this.#ops = [];
  }

  pushOp(op: Mtbon): void {
    this.#ops.push(op);
  }

  takeOps(): Mtbon[] | null {
    const ops = this.#ops;
    if (ops.length > 0) {
      this.#ops = [];
      return ops;
    } else {
      return null;
    }
  }
}


export function compileScript(script: ast.Script, errors: ErrorSink, resolve: VarResolver): Mtbon {
  const cx: Context = new Context(errors, resolve);
  return {
    version: 1,
    nodes: compileNodes(cx, script.nodes),
  };
}

function compileNodes(cx: Context, nodes: Iterable<ast.Node>): CompiledNode[] {
  const compiled: CompiledNode[] = [];

  for (const node of nodes) {
    switch (node.type) {
      case "action": {
        // Combine consecutive non-action statements into one.
        const ops = cx.takeOps();
        if (ops !== null) {
          compiled.push({ name: "exec", params: ops });
        }

        compiled.push(compileAction(cx, node));
        break;
      }

      case "assign": {
        compileVarParts(cx, node.lhs);
        compileExprParts(cx, node.rhs);
        cx.pushOp({
          set: node.op === null ? true : node.op,
          args: node.lhs.parts.length + 2,
        });
        break;
      }

      default: {
        // Expression node.
        compileExprParts(cx, node);
        break;
      }
    }
  }

  // Combine consecutive non-action statements into one.
  const ops = cx.takeOps();
  if (ops !== null) {
    compiled.push({ name: "exec", params: ops });
  }

  return compiled;
}

function compileAction(cx: Context, action: ast.Action): CompiledNode {
  const node: CompiledNode = {
    name: action.name.ident,
    params: compileActionParams(cx, action.params),
    children: null,
  };

  if (action.modifiers.length > 0) {
    compileActionModifers(cx, action.modifiers, node);
  }

  if (action.params.length === 0) {
    delete node.params;
  }

  if (action.children.length > 0) {
    node.children = compileNodes(cx, action.children);
  } else {
    delete node.children;
  }

  return node;
}

function compileActionParams(cx: Context, params: ast.ActionParam[]): Mtbon {
  const seen: Map<string, ast.Ident> = new Map();
  const named: Record<string, Mtbon> = {};
  let anon: { span: Span, val: Mtbon } | undefined = undefined;

  for (const param of params) {
    const val = compileExpr(cx, param.value);
    if (param.name === null) {
      if (anon === undefined) {
        anon = { span: param.value.span, val };
      } else {
        semanticError(cx, "Duplicate anonymous parameter", [param.value.span, anon.span]);
      }
    } else {
      const name = param.name;
      const existing = seen.get(name.ident);
      if (existing === undefined) {
        seen.set(name.ident, name);
        named[name.ident] = val;
      } else {
        semanticError(cx, `Duplicate parameter name: ${name.ident}`, [name.span, existing.span]);
      }
    }
  }

  if (anon !== undefined) {
    if (seen.size > 0) {
      const errSpans = [anon.span, seen.values().next().value!.span];
      semanticError(cx, "An action cannot have both named and anonymous parameters", errSpans);
    }
    return anon.val;
  } else {
    return named;
  }
}

function compileActionModifers(cx: Context, modifiers: ast.NamedParam[], out: Record<string, any>): void {
  const seen: Map<string, ast.Ident> = new Map();

  for (const mod of modifiers) {
    let val: Mtbon | undefined = undefined;
    switch (mod.value.type) {
      case "lit":
        val = mod.value.value;
        break;
      case "const": {
        val = compileExprConst(cx, mod.value);
        break;
      }
      default:
        semanticError(cx, "Modifiers must be literals or constant expressions", [mod.value.span]);
    }

    const name = mod.name;
    if (Object.prototype.hasOwnProperty.call(out, name.ident)) {
      const existing = seen.get(name.ident);
      if (existing === undefined) {
        semanticError(cx, `Invalid modifier name: ${name.ident}`, [name.span]);
      } else {
        semanticError(cx, `Duplicate modifier name: ${name.ident}`, [name.span, existing.span]);
      }
    } else {
      if (val !== undefined) {
        out[name.ident] = val;
      }
      seen.set(name.ident, name);
    }
  }
}

function compileExprConst(cx: Context, expr: ast.ExprConst): Mtbon {
  const val = EvalContext.eval(expr, cx.resolve);
  switch (val.type) {
    case "error":
      cx.error(val);
      return null;
    case "object":
      cx.error({
        type: "error",
        kind: ErrorKind.ConstEval,
        msg: "Constant expressions must evaluate to a primitive value",
        spans: [expr.span],
      });
      return null;
    default:
      return val.value;
  }
}

function compileExpr(cx: Context, expr: ast.Expr): Mtbon {
  switch (expr.type) {
    case "lit": return expr.value;
    case "const": return compileExprConst(cx, expr);
    default: {
      compileExprParts(cx, expr);
      return cx.takeOps();
    }
  }
}

function compileVarParts(cx: Context, expr: ast.ExprVar) {
  cx.pushOp(expr.name.ident);
  for (const part of expr.parts) {
    if (part.type === "field") {
      cx.pushOp(part.name);
    } else {
      compileExprParts(cx, part);
    }
  }
}

function compileExprParts(cx: Context, expr: ast.Expr) {
  switch (expr.type) {
    case "lit":
      cx.pushOp(expr.value);
      break;
    case "binop":
      compileExprParts(cx, expr.lhs);
      compileExprParts(cx, expr.rhs);
      cx.pushOp({ call: expr.op, args: 2 });
      break;
    case "unaryop":
      if (expr.op === "-") {
        // Special case: '-' is actually a binary op so we need to export it as such
        cx.pushOp(0);
        compileExprParts(cx, expr.expr);
        cx.pushOp({ call: "-", args: 2 });
      } else {
        compileExprParts(cx, expr.expr);
        cx.pushOp({ call: expr.op, args: 1 });
      }
      break;
    case "call":
      for (const arg of expr.args) {
        compileExprParts(cx, arg);
      }
      cx.pushOp({ call: expr.func.ident, args: expr.args.length });
      break;
    case "var":
      compileVarParts(cx, expr);
      cx.pushOp({ get: true, args: expr.parts.length + 1 });
      break;
    case "const": {
      cx.pushOp(compileExprConst(cx, expr));
      break;
    }
    default:
      expr satisfies never;
  }
}

function semanticError(cx: Context, msg: string, spans: Span[]): void {
  cx.error({ type: "error", kind: ErrorKind.Semantic, msg, spans });
}
