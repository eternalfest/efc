import * as ast from "./ast.mjs";
import { compileScript as compileScriptInner, Mtbon } from "./compile.mjs";
import { ConstPrimitive, EvalContext, VarResolver } from "./const-eval.mjs";
import { ErrorKind, ErrorSink, Result } from "./error.mjs";
import { parseExprConst, parseScript } from "./parse.mjs";
import { Span } from "./text.mjs";

export * as ast from "./ast.mjs";
export { parseExprConst,parseScript } from "./parse.mjs";

export function compileScript(script: string | ast.Script, errors: ErrorSink, resolve?: VarResolver): Mtbon {
  if (typeof script === "string") {
    const parsed = parseScript(script);
    if (parsed.type === "error") {
      script = {
        type: "script",
        span: Span.ofText(script),
        nodes: [],
      };
      errors(parsed);
    } else {
      script = parsed;
    }
  }

  return compileScriptInner(script, errors, resolve ?? (_ => undefined));
}

export function evalExpr(expr: string | ast.Expr, resolve?: VarResolver): Result<ConstPrimitive> {
  if (typeof expr === "string") {
    const parsed = parseExprConst(expr);
    if (parsed.type === "error") {
      return parsed;
    } else {
      expr = parsed.expr;
    }
  }

  const value = EvalContext.eval(expr, resolve ?? (_ => undefined));
  if (value.type === "object") {
    return {
      type: "error",
      kind: ErrorKind.ConstEval,
      msg: "Constant expressions must evaluate to a primitive value",
      spans: [],
    };
  } else {
    return value;
  }
}
