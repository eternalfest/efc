# 0.6.0 (2024-11-27)

- **[Breaking Change]** Target ES2023
- **[Fix]** Update dependencies
- **[Internal]** Use Node's builtin test framework

# 0.5.1 (2023-03-02)

- **[Fix]** Fix ident-like anonymous parameters not compiling properly.

# 0.5.0 (2023-02-25)

- **[Breaking change]** Replace macro literals by full const-evaluation primitive (`#expr`).
- **[Breaking change]** Rework package to report semantic errors during compiling, and not during parsing.
- **[Feature]** Add 'const expressions', which are evaluated at compile-time:
  - Introduced by the `#` operator: `#foo`, `#foo.bar`, `#foo("bar")`, `#(foo + 1)`;
  - Resolve to literals after compilation;
  - All built-in operators are supported, with few limitations;
  - Allows for providing custom const-only objects or values through variable resolution.
- **[Feature]** Allow parenthesized expressions -- `(...);` -- in statement position.
- **[Change]** Coalesce consecutive assignments in compiled scripts.
- **[Internal]** Drop dependency on `incident`.

# 0.4.0 (2023-01-13)

- **[Breaking change]** Compile library to ESM.
- **[Feature]** Add macro literals (`#foo.bar:baz`), resolving to literals at compile-time.
- **[Internal]** Rework project structure.
- **[Internal]** Update to Yarn 4.

# 0.3.0 (2020-06-29)

- **[Breaking change]** Update to ESM.

# 0.2.1 (2019-12-31)

- **[Internal]** Update dev dependencies.

# 0.2.0 (2019-04-28)

- **[Breaking change]** Spans can be styled independently in `TextMap.getSpansInContext`.
- **[Feature]** Add support for expression and assignment syntax.
- **[Feature]** Add more spans styles for `TextMap`.

# 0.1.0 (2019-01-03)

- **[Feature]** First release.
