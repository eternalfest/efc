# BetterScripts

A Node parser for the BetterScripts mini-language.

The specification can be found [here](https://gitlab.com/eternalfest/specs/blob/master/better-scripts/BetterScript.md).

## License

[AGPL License](./LICENSE.md)
