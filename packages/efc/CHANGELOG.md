# 0.6.0 (2024-11-27)

- **[Breaking Change]** Target ES2023
- **[Fix]** Update dependencies
- **[Internal]** Use Node's builtin test framework

# 0.4.4 (2023-03-03)

- **[Fix]** Correctly load old-style assets folders on Windows.

# 0.4.3 (2023-03-02)

- **[Fix]** Update `@eternalfest/better-scripts` to `0.5.1`.

# 0.4.2 (2023-02-26)

- **[Change]** Warn on duplicate keys in data files when two const-exprs evaluate to the same value.
- **[Fix]** Fix `grid` chunks whose levels use the new-style `portal` (instead of `vortex`) id.
- **[Internal]** Update dependencies.

# 0.4.1 (2023-02-25)

- **[Feature]** Allow data files to use `.json`, `.json5` or `.toml` extensions.
- **[Feature]** Improve error messages.
- **[Feature]** Add constant-evaluated expressions usable in level scripts.
  - They are written `#foo.bar`, `#foo(...)`, or `#(...)`, and can contain
    (almost) arbitrary expressions (e.g. `#foo(5 / 2)` or `#(foo.bar + "baz")`);
  - They are always evaluated during compilation and compiled to a literal value;
  - They can access special 'const-only' variables:
    - `#bad.name`, `#ray.name`, `#tile.name`, `#bg.name`: gives the numerical id of
      a bad, ray, tile, or background, respectively, as defined in the project's assets;
    - `#obfu("name")`: gives the corresponding obfuscated string.
- **[Feature]** Constant-evaluated expressions can also be used inside data files;
  for raw strings starting with a `#`, use `"##..."` instead.
- **[Change]** Deprecate comments in `.json` data files; this will be a hard error in a future release.
- **[Change]** Be stricter when parsing XML files.
- **[Fix]** Properly support aliases across assets directories.

# 0.4.0 (2023-02-10)

- **[Feature]** Add support for new-style (HammerfestCreator 1.8.0+) assets definitions.

# 0.3.5 (2023-01-13)

- **[Feature]** Add `separateLocales` compilation option.
- **[Internal]** Remove unused assets files.
- **[Internal]** Update dependencies.

# 0.3.4 (2022-05-25)

- **[Fix]** Fix patcher data resolution on Windows.

# 0.3.3 (2022-05-09)

- **[Feature]** Warn if two links have the same 'from' endpoint.
- **[Fix]** Properly sort levels inside columns in `type="grid"` lands.
- **[Fix]** Use Yarn's Plug'n'Play linker.
- **[Internal]** Update dependencies.

# 0.3.2 (2022-03-10)

- **[Fix]** Update dependencies, drop `rxjs` patch.

# 0.3.1 (2021-10-16)

- **[Fix]** Include `rxjs` patch in published package.

# 0.3.0 (2021-10-16)

- **[Breaking change]** Compile library to ESM, drop `./lib` prefix from deep imports.
- **[Feature]** Improve error messages.
- **[Feature]** Warn on level tags that aren't lowercase.
- **[Fix]** Update dependencies

# 0.2.0 (2020-01-29)

- **[Breaking change]** Update level set configuration.
- **[Feature]** Add support for obfuscation.
- **[Internal]** Fix continuous deployment script.

# 0.1.11 (2019-12-25)

- **[Fix]** Update dependencies.

# 0.1.10 (2019-12-31)

- **[Fix]** Update dependencies.

# 0.1.9 (2019-11-24)

- **[Fix]** Fix issue with dimension separators on level sets with `hasPaddingLevel == false`.

# 0.1.8 (2019-11-20)

- **[Feature]** Add support for multiple lands in Time Attack level sets.
- **[Feature]** Add support for `fjv` level set.
- **[Fix]** Don't emit tags in `game.xml` for unused level sets.

# 0.1.7 (2019-09-05)

- **[Feature]** Add level set signatures.
- **[Fix]** Emit error on invalid asset file.

# 0.1.6 (2019-05-15)

- **[Fix]** Report warning (instead of error) on missing `<player>` tag.

# 0.1.5 (2019-05-15)

- **[Fix]** Add support for levels without `<script>`.
- **[Fix]** Fix crash in `cheerioWriter.removeCheerioWhitespace`.
- **[Fix]** Fix vortex detection for grid chunks.
- **[Fix]** Update dependencies.

# 0.1.4 (2019-04-29)

- **[Feature]** Update dependencies, notably update to `@eternalfest/better-script@2`.
- **[Feature]** Add support for grid chunks.
- **[Feature]** Add support for asset aliases.

# 0.1.3 (2019-02-05)

- **[Fix]** Pretty print `game.xml`.
- **[Fix]** Add support for levels without `<player>`.
- **[Fix]** Fix dimension separator.
- **[Internal]** Refactor `compiler-result` module.

# 0.1.2 (2019-01-23)

- **[Feature]** Add support for advanced tags.
- **[Feature]** Add support for custom assets.
- **[Fix]** Include assets in `lib` builds.

# 0.1.1 (2019-01-08)

- **[Fix]** Fix `--version` command.

# 0.1.0 (2019-01-08)

- **[Feature]** First release.
