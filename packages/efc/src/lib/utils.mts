import * as rx from "rxjs";
import { switchMap as rxSwitchMap } from "rxjs/operators";
import { URL } from "url";

import { CompilerResult as Cr, error as crError } from "./compiler-result.mjs";
import * as diags from "./diagnostic/errors.mjs";
import { isDirectoryAsync, readTextFileAsync } from "./fs-utils.mjs";
import { getObservableFileVersion } from "./observable-fs.mjs";

export type Resolvable<T> = T | PromiseLike<T>;

export function withObservedTextFile<R>(fileUri: URL, f: (text: string) => Resolvable<Cr<R>>): rx.Observable<Cr<R>> {
  Promise.resolve();
  return getObservableFileVersion(fileUri)
    .pipe(rxSwitchMap(() => withTextFile(fileUri, f)));
}

export async function withTextFile<R>(fileUri: URL, f: (text: string) => Resolvable<Cr<R>>): Promise<Cr<R>> {
  let text: string;
  try {
    text = await readTextFileAsync(fileUri);
  } catch (err) {
    return crError(diags.fsIoError(err as NodeJS.ErrnoException));
  }
  return addSourceToDiagnostics(await f(text), fileUri, text);
}

export async function withTextFileOrDir<R>(fileUri: URL, f: (text: string | null) => Resolvable<Cr<R>>): Promise<Cr<R>> {
  let text: string | null;
  try {
    text = await readTextFileAsync(fileUri);
  } catch (err) {
    // We can't test for `EISDIR` because Windows doesn't return this error code.
    if (await isDirectoryAsync(fileUri)) {
      text = null;
    } else {
      return crError(diags.fsIoError(err as NodeJS.ErrnoException));
    }
  }
  return addSourceToDiagnostics(await f(text), fileUri, text ?? undefined);
}

function addSourceToDiagnostics<T>(cr: Cr<T>, fileUri: URL, sourceText?: string): Cr<T> {
  return cr.mapDiagnostics(d => ({
    ...d,
    fileUri: d.fileUri ?? fileUri,
    sourceText: d.sourceText ?? sourceText,
  }));
}

const CHAR_UPPER_A: number = 0x41;
const CHAR_UPPER_Z: number = 0x5A;
const CHAR_LOWER_A: number = 0x61;
const CHAR_LOWER_Z: number = 0x7A;
const CHAR_UNDERSCORE: number = 0x5F;
const CHAR_0: number = 0x30;
const CHAR_9: number = 0x39;

export function isAsciiIdentifierStart(code: number): boolean {
  return (code >= CHAR_UPPER_A && code <= CHAR_UPPER_Z)
    || (code >= CHAR_LOWER_A && code <= CHAR_LOWER_Z)
    || code === CHAR_UNDERSCORE;
}

export function isAsciiIdentifierContinue(code: number): boolean {
  return isAsciiIdentifierStart(code) || (code >= CHAR_0 && code <= CHAR_9);
}

/**
 * Mark a block of code as unreachable.
 *
 * Reaching it will throw an error reporting the broken assertion.
 *
 * The witness value is used to exhibit the broken typesystem invariant
 * (useful in switch/case statements).
 */
export function unreachable(_witness: never, message?: string, ): never {
  if (typeof  message === "string") {
    throw new Error(`Entered unreachable code: ${message}`);
  } else {
    throw new Error("Entered unreachable code");
  }
}
