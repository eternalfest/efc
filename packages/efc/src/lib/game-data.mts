import { Buffer } from "node:buffer";

import { Cheerio, CheerioAPI, load as cheerioLoad } from "cheerio";
import { createHash } from "crypto";
import { Element } from "domhandler";

import { cheerioToString } from "./cheerio-writer.mjs";
import { emitLevelPort, LevelPort, LevelPos } from "./compiler/links.mjs";
import { LandsLocale } from "./locales/lands-locale.mjs";
import { Locale } from "./locales/locale.mjs";
import { unreachable } from "./utils.mjs";

export interface GameData {
  readonly levelSets: ReadonlyMap<string, OutLevelSet>;
  readonly patcherData: ReadonlyMap<string, any>;
  readonly specialItems: Cheerio<Element>;
  readonly scoreItems: Cheerio<Element>;
  readonly links: Links;
  readonly locales: ReadonlyMap<string, Locale>;
}

export interface OutLevelSet {
  readonly varName: string;
  readonly mtbon: string;
}

const CHEERIO_XML: CheerioAPI = cheerioLoad("", {xml: true});

export interface Links {
  readonly tags: ReadonlyMap<string, LevelPos>;
  readonly ways: ReadonlyArray<Way>;
}

export interface Way {
  readonly type: "one-way" | "two-way";
  readonly from: LevelPort;
  readonly to: LevelPort;
}

export function stringifyGameData(gameData: GameData, prettyXml: boolean = false): string {
  return cheerioToString(emitGameData(gameData), prettyXml);
}

export function stringifyLocale(localeId: string, locale: Locale, prettyXml: boolean = false): string {
  return cheerioToString(emitLocale(localeId, locale), prettyXml);
}

export function emitGameData(gameData: GameData): Cheerio<Element> {
  const result: Cheerio<Element> = CHEERIO_XML<Element, string>("<game>");
  result.append(emitLevelSets(gameData.levelSets));
  result.append(emitPatcherData(gameData.patcherData));
  result.append(emitLinks(gameData.links));
  result.append(gameData.specialItems);
  result.append(gameData.scoreItems);
  result.append(CHEERIO_XML("<quests />"));
  for (const [localeId, locale] of gameData.locales) {
    result.append(emitLocale(localeId, locale));
  }
  return result;
}

export function emitLevelSets(levelSets: ReadonlyMap<string, OutLevelSet>): Cheerio<Element> {
  const result: Cheerio<Element> = CHEERIO_XML<Element, string>("<levels>");
  for (const {varName, mtbon} of levelSets.values()) {
    result.append(emitLevelSet(varName, mtbon));
  }
  return result;
}

export function emitLevelSet(varName: string, mtbon: string): Cheerio<Element> {
  const signatures: LevelSetSignatures = getLevelSetSignatures(varName, mtbon);

  const levelSet: Cheerio<Element> = CHEERIO_XML<Element, string>("<dim>")
    .attr("name", signatures.name)
    .attr("signature", signatures.signature);

  if (mtbon !== "") {
    levelSet.text(mtbon);
  }

  return levelSet;
}

interface LevelSetSignatures {
  name: string;
  signature: string;
}

function getLevelSetSignatures(varName: string, data: string): LevelSetSignatures {
  return {
    name: varName,
    signature: getLevelSetSignature(varName, data),
  };
}

function getLevelSetSignature(name: string, data: string): string {
  const c: number = name.charCodeAt(3);
  let value: number = 0;
  for (const part of data.split("0")) {
    if (part.length > 30) {
      value += part.charCodeAt(c % 5);
      value += part.charCodeAt(c % 9) * part.charCodeAt(c % 15);
      value += part.charCodeAt(c % 19);
      value += part.charCodeAt(c % 22);
    }
  }

  return `${md5(name)}${md5(value.toString(10))}`;
}

function md5(data: string): string {
  return createHash("md5")
    .update(Buffer.from(data, "utf8"))
    .digest("hex");
}

export function emitPatcherData(patcherData: ReadonlyMap<string, OutLevelSet>): Cheerio<Element> {
  const result: Cheerio<Element> = CHEERIO_XML<Element, string>("<datas>");
  for (const [key, value] of patcherData) {
    result.append(CHEERIO_XML("<data>").attr("name", key).text(JSON.stringify(value)));
  }
  return result;
}

export function emitLinks(links: Links): Cheerio<Element> {
  return CHEERIO_XML<Element, string>("<links>")
    .append(emitTags(links.tags))
    .append(emitWays(links.ways));
}

export function emitTags(tags: ReadonlyMap<string, LevelPos>): Cheerio<Element> {
  const node: Cheerio<Element> = CHEERIO_XML<Element, string>("<tags>");
  for (const [key, pos] of tags) {
    node.append(
      CHEERIO_XML("<tag>")
        .attr("name", key)
        .attr("did", pos.did)
        .attr("lid", pos.levelId.toString(10)),
    );
  }
  return node;
}

export function emitWays(ways: Iterable<Way>): Cheerio<Element> {
  const node: Cheerio<Element> = CHEERIO_XML<Element, string>("<ways>");
  for (const way of ways) {
    node.append(emitWay(way));
  }
  return node;
}

export function emitWay(way: Way): Cheerio<Element> {
  const wayType = way.type;
  let node: Cheerio<Element>;
  switch (wayType) {
    case "one-way":
      node = CHEERIO_XML<Element, string>("<oneway>");
      break;
    case "two-way":
      node = CHEERIO_XML<Element, string>("<twoway>");
      break;
    default:
      return unreachable(wayType, "Unexpected Way.type");
  }
  return node
    .attr("from", emitLevelPort(way.from))
    .attr("to", emitLevelPort(way.to));
}

export function emitLocale(localeId: string, locale: Locale): Cheerio<Element> {
  return CHEERIO_XML<Element, string>("<lang>")
    .attr("id", localeId)
    .attr("debug",  "0")
    .append(locale.statics)
    .append(locale.items)
    .append(emitLandsLocale(locale.lands))
    .append(locale.keys)
    .append(CHEERIO_XML("<quests />"))
    .append(CHEERIO_XML("<families />"));
}

export function emitLandsLocale(locale: LandsLocale): Cheerio<Element> {
  const node: Cheerio<Element> = CHEERIO_XML<Element, string>("<dimensions>");
  for (const [setId, levels] of locale.getNames()) {
    const dimNode: Cheerio<Element> = CHEERIO_XML<Element, string>("<dimension>");
    dimNode.attr("id", setId);

    const list: [number, string][] = [...levels.entries()];
    list.sort((a, b) => a[0] - b[0]); // Sort by ascending order
    for (const [levelId, name] of list) {
      dimNode.append(
        CHEERIO_XML("<level>")
          .attr("id", levelId.toString(10))
          .attr("name", name),
      );
    }
    node.append(dimNode);
  }
  return node;
}
