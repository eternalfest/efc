import { Observable, of as rxOf, OperatorFunction } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import { Diagnostic } from "./diagnostic/report.mjs";

export interface CompilerResultLike<T> {
  readonly diagnostics: ReadonlyArray<Diagnostic>;
  readonly value: T | typeof NO_VALUE;
}

export interface CompilerResultLikeWithValue<T> {
  readonly diagnostics: ReadonlyArray<Diagnostic>;
  readonly value: T;
}

export type CompilerResultRecord<T> = { [P in keyof T]: CompilerResult<T[P]> };

export const NO_VALUE: unique symbol = Symbol("NO_VALUE");

export class CompilerResult<T> {
  public readonly diagnostics: ReadonlyArray<Diagnostic>;
  public readonly value: T | typeof NO_VALUE;

  public constructor(value: T | typeof NO_VALUE, diagnostics: ReadonlyArray<Diagnostic>) {
    this.diagnostics = diagnostics;
    this.value = value;
  }

  public map<R>(project: (value: T) => R): CompilerResult<R> {
    return map(this, project);
  }

  public async mapAsync<R>(project: (value: T) => Promise<R>): Promise<CompilerResult<R>> {
    return mapAsync(this, project);
  }

  public flatMap<R>(project: (value: T) => CompilerResultLike<R>): CompilerResult<R> {
    return flatMap(this, project);
  }

  public async flatMapAsync<R>(project: (value: T) => Promise<CompilerResultLike<R>>): Promise<CompilerResult<R>> {
    return flatMapAsync(this, project);
  }

  public mapDiagnostics(project: (value: Diagnostic) => Diagnostic | undefined): CompilerResult<T> {
    return mapDiagnostics(this, project);
  }

  public flatTap(f: (cr: this) => void): this {
    f(this);
    return this;
  }

  public hasValue(): boolean {
    return hasValue(this);
  }

  public unwrapOr(defaultVal: T): T {
    return unwrapOr(this, defaultVal);
  }

  public unwrap(): T {
    return unwrap(this);
  }
}

export function error(diagnostic: Diagnostic): CompilerResult<never> {
  return new CompilerResult<never>(NO_VALUE, [diagnostic]);
}

export function warn<T>(value: T, diagnostic: Diagnostic): CompilerResult<T> {
  return new CompilerResult(value, [diagnostic]);
}

export function errors(diagnostic: ReadonlyArray<Diagnostic>): CompilerResult<never> {
  return new CompilerResult<never>(NO_VALUE, diagnostic);
}

export function join<T>(crs: CompilerResultRecord<T>): CompilerResult<T> {
  const diagnostics: Diagnostic[] = [];
  const result: Partial<T> = {};
  let noValue: boolean = false;
  for (const [key, cr] of (Object.entries(crs) as [string, CompilerResult<any>][])) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if (cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      Reflect.set(result, key, cr.value);
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : (result as T), diagnostics);
}

export function joinArray<T0>(crs: [CompilerResultLike<T0>]): CompilerResult<[T0]>;
export function joinArray<T0, T1>(crs: [CompilerResultLike<T0>, CompilerResultLike<T1>]): CompilerResult<[T0, T1]>;
export function joinArray<T>(crs: Iterable<CompilerResultLike<T>>): CompilerResult<T[]>;
export function joinArray<T>(crs: Iterable<CompilerResultLike<T>>): CompilerResult<T[]> {
  const diagnostics: Diagnostic[] = [];
  const result: T[] = [];
  let noValue: boolean = false;
  for (const cr of crs) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if (cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      result.push(cr.value);
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : result, diagnostics);
}

// export function joinArray<T0>(crs: [CompilerResult<T0>]): CompilerResult<[T0]>;
// export function joinArray<T0, T1>(crs: [CompilerResult<T0>, CompilerResult<T1>]): CompilerResult<[T0, T1]>;
export async function joinArrayAsync<T>(crs: AsyncIterable<CompilerResultLike<T>>): Promise<CompilerResult<T[]>> {
  const diagnostics: Diagnostic[] = [];
  const result: T[] = [];
  let noValue: boolean = false;
  for await (const cr of crs) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if (cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      result.push(cr.value);
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : result, diagnostics);
}

export function joinMap<K, V>(
  crs: Iterable<[K, CompilerResultLike<V>]>,
  merge?: (key: K, oldVal: V, newVal: V) => CompilerResultLike<V>,
): CompilerResult<Map<K, V>> {
  function* entries(crs: Iterable<[K, CompilerResultLike<V>]>): Iterable<CompilerResultLike<[K, V]>> {
    for (const [key, cr] of crs) {
      yield map(cr,val => [key, val]);
    }
  }
  return joinMapEntries(entries(crs), merge);
}

export function joinMapEntries<K, V>(
  crs: Iterable<CompilerResultLike<[K, V]>>,
  merge?: (key: K, oldVal: V, newVal: V) => CompilerResultLike<V>,
): CompilerResult<Map<K, V>> {
  const diagnostics: Diagnostic[] = [];
  const result: Map<K, V> = new Map();
  let noValue: boolean = false;
  for (const cr of crs) {
    for (const diag of cr.diagnostics) {
      diagnostics.push(diag);
    }
    if(cr.value === NO_VALUE) {
      noValue = true;
    } else if (!noValue) {
      const [key, val] = cr.value;
      if (merge !== undefined && result.has(key)) {
        const old: V = result.get(key)!;
        const merged: CompilerResultLike<V> = merge(key, old, val);
        for (const mergedDiag of merged.diagnostics) {
          diagnostics.push(mergedDiag);
        }
        if (merged.value === NO_VALUE) {
          noValue = true;
        } else {
          result.set(key, merged.value);
        }
      } else {
        result.set(key, val);
      }
    }
  }
  return new CompilerResult(noValue ? NO_VALUE : result, diagnostics);
}

export function map<T, R>(cr: CompilerResultLike<T>, project: (value: T) => R): CompilerResult<R> {
  return cr.value === NO_VALUE
    ? from(cr) as CompilerResult<never>
    : new CompilerResult(project(cr.value), cr.diagnostics);
}

export function mapDiagnostics<T>(
  cr: CompilerResultLike<T>,
  project: (value: Diagnostic) => Diagnostic | undefined,
): CompilerResult<T> {
  const newDiagnostics: Diagnostic[] = [];
  for (const diagnostic of cr.diagnostics) {
    const projected: Diagnostic | undefined = project(diagnostic);
    if (projected !== undefined) {
      newDiagnostics.push(projected);
    }
  }
  return new CompilerResult(cr.value, newDiagnostics);
}

export async function mapAsync<T, R>(
  cr: CompilerResultLike<T>,
  project: (value: T) => Promise<R>,
): Promise<CompilerResult<R>> {
  return cr.value === NO_VALUE
    ? from(cr) as CompilerResult<never>
    : new CompilerResult(await project(cr.value), cr.diagnostics);
}

export function flatMap<T, R>(
  cr: CompilerResultLike<T>,
  project: (value: T) => CompilerResultLike<R>,
): CompilerResult<R> {
  if (cr.value === NO_VALUE) {
    return from(cr) as CompilerResult<never>;
  }
  const newCr: CompilerResultLike<R> = project(cr.value);
  const diagnostics: Diagnostic[] = [...cr.diagnostics];
  for (const diagnostic of newCr.diagnostics) {
    diagnostics.push(diagnostic);
  }
  return new CompilerResult(newCr.value, diagnostics);
}

export async function flatMapAsync<T, R>(
  cr: CompilerResultLike<T>,
  project: (value: T) => Promise<CompilerResultLike<R>>,
): Promise<CompilerResult<R>> {
  if (cr.value === NO_VALUE) {
    return from(cr) as CompilerResult<never>;
  }
  const newCr: CompilerResultLike<R> = await project(cr.value);
  const diagnostics: Diagnostic[] = [...cr.diagnostics];
  for (const diagnostic of newCr.diagnostics) {
    diagnostics.push(diagnostic);
  }
  return new CompilerResult(newCr.value, diagnostics);
}

export function rxFlatMapCr<T, R>(
  project: (value: T) => CompilerResultLike<R>,
): OperatorFunction<CompilerResultLike<T>, CompilerResult<R>> {
  return rxMap((value: CompilerResultLike<T>): CompilerResult<R> => {
    return flatMap(value, project);
  });
}

export function flattenRx<T>(
  crObs: CompilerResultLike<Observable<CompilerResultLike<T>>>,
): Observable<CompilerResult<T>> {
  if (crObs.value === NO_VALUE) {
    return rxOf(new CompilerResult<T>(NO_VALUE, crObs.diagnostics));
  }

  return crObs.value.pipe(
    rxMap((cr: CompilerResultLike<T>): CompilerResult<T> => {
      const diags: Diagnostic[] = crObs.diagnostics.concat(cr.diagnostics);
      return new CompilerResult(cr.value, diags);
    }),
  );
}

export function of<T>(value: T): CompilerResult<T> {
  return new CompilerResult(value, []);
}

export function from<T>(cr: CompilerResultLike<T>): CompilerResult<T> {
  return cr instanceof CompilerResult ? cr : new CompilerResult(cr.value, cr.diagnostics);
}

export function hasValue<T>(cr: CompilerResultLike<T>): cr is CompilerResultLikeWithValue<T> {
  return cr.value !== NO_VALUE;
}

export function unwrapOr<T>(cr: CompilerResultLike<T>, defaultVal: T): T {
  if (cr.value === NO_VALUE) {
    return defaultVal;
  }
  return cr.value;
}

export function unwrap<T>(cr: CompilerResultLike<T>): T {
  if (cr.value === NO_VALUE) {
    throw new Error("UnwrappingCompilerResultWithoutValue");
  }
  return cr.value;
}
