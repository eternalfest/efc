import { Cheerio,CheerioAPI, load as cheerioLoad } from "cheerio";
import { Element, ParentNode } from "domhandler";

import { CompilerResult as Cr, error as crError, of as crOf } from "./compiler-result.mjs";
import { xmlChildNonUnique, xmlChildNotFound, xmlInvalidAttribute, xmlMissingAttribute } from "./diagnostic/errors.mjs";

export function loadXml(text: string, withoutSpans: boolean = false): CheerioAPI {
  return cheerioLoad(
    text,
    {xml: {withStartIndices: !withoutSpans, withEndIndices: !withoutSpans}},
  );
}

export function readOneChild(node: Cheerio<ParentNode>, selector: string): Cr<Cheerio<Element>> {
  const children: Cheerio<Element> = node.children(selector);
  if (children.length === 1) {
    return crOf(children);
  }
  return crError(children.length === 0 ? xmlChildNotFound(node, selector) : xmlChildNonUnique(children[1], selector));
}

export function readOneChildWithFallback(node: Cheerio<ParentNode>, selector: string, fallbackSelector: string): Cr<Cheerio<Element>> {
  let children: Cheerio<Element> = node.children(selector);
  if (children.length === 0) {
    children = node.children(fallbackSelector);
  }
  if (children.length === 1) {
    return crOf(children);
  }
  return crError(children.length === 0 ? xmlChildNotFound(node, selector) : xmlChildNonUnique(children[1], selector));
}

export function readOneOrNoneChild(node: Cheerio<Element>, selector: string): Cr<Cheerio<Element> | undefined> {
  const children: Cheerio<Element> = node.children(selector);
  if (children.length <= 1) {
    return crOf(children.length === 0 ? undefined : children);
  }
  return crError(xmlChildNonUnique(children[1], selector));
}

export function cheerioMapChildren<R>(
  $: CheerioAPI,
  node: Cheerio<Element>,
  selector: string,
  fn: (child: Cheerio<Element>) => R,
): R[] {
  const result: R[] = [];
  node
    .children(selector)
    .each((_: number, element: Element): void => {
      result.push(fn($(element)));
    });
  return result;
}

export function readAttr(node: Cheerio<Element>, attrName: string): Cr<string> {
  const value: string | undefined = node.attr(attrName);
  if (value !== undefined) {
    return crOf(value);
  }
  return crError(xmlMissingAttribute(node, attrName));
}

export function readAttrWithFallback(node: Cheerio<Element>, attrName: string, fallbackName: string): Cr<string> {
  const value: string | undefined = node.attr(attrName);
  if (value !== undefined) {
    return crOf(value);
  }
  const fallbackValue: string | undefined = node.attr(fallbackName);
  if (fallbackValue !== undefined) {
    return crOf(fallbackValue);
  }
  return crError(xmlMissingAttribute(node, attrName));
}

export function readFloatAttr(node: Cheerio<Element>, attrName: string): Cr<number> {
  const valueStr: string | undefined = node.attr(attrName);
  if (valueStr === undefined) {
    return crError(xmlMissingAttribute(node, attrName));
  }

  const value: number = parseFloat(valueStr);
  if (isNaN(value)) {
    return crError(xmlInvalidAttribute(node, attrName, valueStr));
  }

  return crOf(value);
}
