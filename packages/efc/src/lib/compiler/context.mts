import * as bs from "@eternalfest/better-scripts/const-eval";

import * as diags from "../diagnostic/errors.mjs";
import { JavaAssetRef, JavaAssets } from "../java/assets.mjs";
import { ObfFn } from "./obf.mjs";

export interface CompilerContext {
  readonly assets: JavaAssets;
  readonly obf: ObfFn;
  readonly resolver: bs.VarResolver;
}

export function createCompilerContext(assets: JavaAssets, obf: ObfFn): CompilerContext {
  return new CompilerContextImpl(assets, obf);
}

class CompilerContextImpl implements CompilerContext {
  readonly assets: JavaAssets;
  readonly obf: ObfFn;
  readonly resolver: bs.VarResolver;

  #bads: bs.ConstObject;
  #bgs: bs.ConstObject;
  #tiles: bs.ConstObject;
  #rays: bs.ConstObject;
  #obfu: bs.ConstObject;

  constructor(assets: JavaAssets, obf: ObfFn) {
    this.assets = assets;
    this.obf = obf;
    this.resolver = this.#resolveVar.bind(this);

    this.#bads = new BsAssetMap("bad", assets.bads, "bad_");
    this.#bgs = new BsAssetMap("background", assets.backgrounds);
    this.#tiles = new BsAssetMap("tile", assets.tiles);
    this.#rays = new BsAssetMap("ray", assets.fields);
    this.#obfu = new BsObfu(obf);
  }

  #resolveVar(name: string): bs.ConstValue | undefined {
    switch (name) {
      case "bad": return this.#bads;
      case "bg": return this.#bgs;
      case "tile": return this.#tiles;
      case "ray": return this.#rays;
      case "obfu": return this.#obfu;
      default: return undefined;
    }
  }
}

class BsObfu implements bs.ConstObject {
  type = "object" as const;
  #obf: ObfFn;

  constructor(obf: ObfFn) {
    this.#obf = obf;
  }

  call(cx: bs.EvalContext, args: bs.ConstValue[]): bs.ConstValue {
    const [name] = cx.expectArgs(args, 1);
    const obfu: string = this.#obf(cx.expect(name, "string"));
    return cx.string(obfu);
  }
}

class BsAssetMap implements bs.ConstObject {
  type = "object" as const;
  #kind: diags.AssetType;
  #prefix?: string;
  #map: ReadonlyMap<string, JavaAssetRef>;

  constructor(kind: diags.AssetType, map: ReadonlyMap<string, JavaAssetRef>, prefix?: string) {
    this.#kind = kind;
    this.#prefix = prefix;
    this.#map = map;
  }

  getField(cx: bs.EvalContext, name: string): bs.ConstValue {
    if (this.#prefix !== undefined) {
      name = this.#prefix + name;
    }

    const asset: JavaAssetRef | undefined = this.#map.get(name);
    // Hidden aliases aren't accessible via asset tags.
    if (asset === undefined || asset.hidden) {
      cx.error(diags.assetNotFound(this.#kind, name).message);
    }

    const value: number | undefined = asset.ref.value;
    if (value === undefined) {
      cx.error(diags.missingAssetValue(this.#kind, name).message);
    }

    return cx.number(value);
  }
}
