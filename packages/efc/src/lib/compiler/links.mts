import { CACHE } from "../cache.mjs";
import { Chunk, getChunkLength } from "../chunk.mjs";
import { CompilerResult as Cr, error as crError, join as crJoin, of as crOf } from "../compiler-result.mjs";
import * as diags from "../diagnostic/errors.mjs";
import { Diagnostic } from "../diagnostic/report.mjs";
import { isAsciiIdentifierContinue, isAsciiIdentifierStart, unreachable } from "../utils.mjs";
import { computeGridInfo, getInGrid, GridChunkInfo, GridPos } from "./grid.mjs";
import { LevelSet } from "./level-set.mjs";

export interface LevelPos {
  readonly refType: "pos";
  readonly did: string;
  readonly levelId: number;
}

export type LevelRefPos = LevelPos | LevelRef;
export type LevelRef = LevelRefTag | LevelRefSimple | LevelRefEnd | LevelRefGrid;

export interface LevelRefTag {
  readonly refType: "tag";
  readonly tag: string;
}

export interface LevelRefSimple {
  readonly refType: "simple";
  readonly tag: string;
  readonly offset: number;
}

export interface LevelRefEnd {
  readonly refType: "end";
  readonly tag: string;
}

export interface LevelRefGrid {
  readonly refType: "grid";
  readonly tag: string;
  readonly pos: GridPos;
}

export interface LevelPort<LR extends LevelRef = LevelRef> {
  readonly level: LR;
  readonly port?: number;
}

const LEVEL_GRID_REF_REGEX: RegExp = /^\[([+-]?\d+);([+-]?\d+)\]$/;
const LEVEL_REF_REGEX: RegExp = /^[+-]\d+$/;

export function parseLevelRefExpr(expr: string): Cr<LevelRef> {
  if (expr.length === 0 || !isAsciiIdentifierStart(expr.charCodeAt(0))) {
    return crError(diags.error(`InvalidLevelRef: ${expr}`));
  }
  let idx: number = 1;
  while (idx < expr.length && isAsciiIdentifierContinue(expr.charCodeAt(idx))) {
    idx++;
  }

  const tag: string = expr.substring(0, idx);
  const rest: string = expr.substring(idx);
  if (rest === "") {
    return crOf({refType: "tag", tag});
  } else if (rest === ":end") {
    return crOf({refType: "end", tag});
  } else if (LEVEL_REF_REGEX.test(rest)) {
    return crOf({refType: "simple", tag, offset: parseInt(rest, 10)});
  }
  const match: RegExpExecArray | null = LEVEL_GRID_REF_REGEX.exec(rest);
  if (match === null) {
    return crError(diags.error(`InvalidLevelRef: ${expr}`));
  }

  return crOf({
    refType: "grid",
    tag,
    pos: {x: parseInt(match[1], 10), y: parseInt(match[2], 10)},
  });
}

export function parseLevelPortExpr(expr: string): Cr<LevelPort> {
  if (expr.endsWith(")")) {
    const idx: number | undefined = expr.lastIndexOf("(");
    const portRaw: number = idx === undefined ? NaN : parseInt(expr.substring(idx + 1, expr.length - 1), 10);
    const port: Cr<number> = isNaN(portRaw) ? crError(diags.error(`InvalidLevelPort: ${expr}`)) : crOf(portRaw);
    const level: Cr<LevelRef> = parseLevelRefExpr(expr.substring(0, idx));

    return crJoin({ port, level });
  }

  return parseLevelRefExpr(expr).map(level => ({level, port: undefined}));
}

export function emitLevelPort({level, port}: LevelPort): string {
  const levelExpr: string = emitLevelRef(level);
  if (port === undefined) {
    return levelExpr;
  } else {
    return `${levelExpr}(${port})`;
  }
}

function emitLevelRef(expr: LevelRef): string {
  switch (expr.refType) {
    case "tag":
      return expr.tag;
    case "simple":
      return expr.offset === 0 ? expr.tag : `${expr.tag}+${expr.offset}`;
    case "end":
      return `${expr.tag}:end`;
    case "grid":
      return `${expr.tag}[${expr.pos.x};${expr.pos.y}]`;
    default:
      return unreachable(expr, "Unexpected LevelRef.type");
  }
}

export class LevelRefMap {
  private readonly _tags: Map<string, LevelPos>;
  private readonly _chunks: Map<string, Chunk>;

  private constructor() {
    this._tags = new Map();
    this._chunks = new Map();
  }

  public static make(
    levelSets: ReadonlyMap<string, LevelSet>,
    extraTags: ReadonlyMap<string, LevelRefPos>,
  ): Cr<LevelRefMap> {
    const self: LevelRefMap = new LevelRefMap();
    for (const levelSet of levelSets.values()) {
      let levelId: number = levelSet.hasPaddingLevel ? 1 : 0;
      for (const chunk of levelSet.chunks) {
        levelId = self.processChunk(levelSet.did, levelId, chunk);
        // Skip separator
        levelId += 1;
      }
    }

    // The iteration order of this map is actually important
    const tagDiags: Diagnostic[] = [];
    for (const [name, ref] of extraTags) {
      switch (ref.refType) {
        case "pos":
          self._tags.set(name, ref);
          break;

        default: {
          const resolved: Cr<LevelPos> = self.resolve(ref).map(resolved => {
            self._tags.set(name, resolved);
            return resolved;
          });
          for (const diag of resolved.diagnostics) {
            tagDiags.push(diag);
          }
          break;
        }
      }
    }

    return new Cr(self, tagDiags);
  }

  public get tags(): ReadonlyMap<string, LevelPos> {
    return this._tags;
  }

  public desugar(ref: LevelRef): Cr<LevelRefSimple> {
    switch (ref.refType) {
      case "simple":
        return this.checkTag(ref.tag).map(_ => ref);

      case "tag":
        return this.checkTag(ref.tag).map(_ => ({
          refType: "simple",
          tag: ref.tag,
          offset: 0,
        }));

      case "end":
        return this.getChunkForTag(ref.tag).map(chunk => {
          const lastLevel: number = Math.max(getChunkLength(chunk) - 1, 0);
          return {refType: "simple", tag: ref.tag, offset: lastLevel};
        });

      case "grid":
        return this.getChunkForTag(ref.tag).flatMap(chunk => {
          if (chunk.type !== "grid") {
            return crError(diags.error(`TagGridInvalidChunkType: ${ref.tag}`));
          }
          const info: GridChunkInfo = CACHE.get(chunk, c => computeGridInfo(c.levels));
          const index: number | undefined = getInGrid(info.indices, ref.pos);

          if (index === undefined) {
            return crError(diags.error(`TagGridInvalidPos: ${ref.pos.x}${ref.pos.y}`));
          }
          return crOf({refType: "simple", tag: ref.tag, offset: index});
        });

      default:
        return unreachable(ref, "Unexpected LevelRef.type");
    }
  }

  public desugarWithPort(ref: LevelPort): Cr<LevelPort> {
    return this.desugar(ref.level).map(level => ({level, port: ref.port}));
  }

  public resolve(ref: LevelRefPos): Cr<LevelPos> {
    if (ref.refType === "pos") {
      return crOf({refType: "pos", did: ref.did, levelId: ref.levelId});
    }

    return this.desugar(ref).flatMap(resolved => {
      const tagged: LevelPos | undefined = this._tags.get(resolved.tag);
      if (tagged === undefined) {
        // this should never happen
        return crError(diags.error(`InvalidResolvedTag: ${resolved.tag}`));
      }

      return crOf({refType: "pos", did: tagged.did, levelId: tagged.levelId + resolved.offset});
    });
  }

  private checkTag(tag: string): Cr<LevelPos> {
    const tagged: LevelPos | undefined = this._tags.get(tag);
    if (tagged === undefined) {
      return crError(diags.error(`TagNotFound: ${tag}`));
    }
    return crOf(tagged);
  }

  private getChunkForTag(tag: string): Cr<Chunk> {
    return this.checkTag(tag).flatMap(_ => {
      const chunk: Chunk | undefined = this._chunks.get(tag);
      if (chunk === undefined) {
        return crError(diags.error(`TagEndNoChunk: ${tag}`));
      }
      return crOf(chunk);
    });
  }

  private processChunk(did: string | null, levelId: number, chunk: Chunk): number {
    if (chunk.type === "transient") {
      for (const child of chunk.children) {
        levelId = this.processChunk(did, levelId, child);
      }
    } else {
      if (chunk.firstLevelTag !== undefined) {
        this._chunks.set(chunk.firstLevelTag, chunk);
        if (did !== null) {
          this._tags.set(chunk.firstLevelTag, {refType: "pos", did, levelId});
        }
      }
      levelId += getChunkLength(chunk);
    }
    return levelId;
  }
}
