import * as bs from "@eternalfest/better-scripts";
import { ScriptError as BsScriptError } from "@eternalfest/better-scripts/error";

import {
  CompilerResult as Cr,
  error as crError,
  join as crJoin,
  joinArray as crJoinArray,
  of as crOf,
} from "../compiler-result.mjs";
import * as diags from "../diagnostic/errors.mjs";
import { Diagnostic } from "../diagnostic/report.mjs";
import { JavaAsset, JavaAssetRef } from "../java/assets.mjs";
import * as java from "../java/level.mjs";
import { unreachable } from "../utils.mjs";
import { CompilerContext } from "./context.mjs";
import * as hf from "./hf.mjs";
import { Vector2D } from "./vector-2d.mjs";

export function javaToHfLevel(cx: CompilerContext, javaLevel: java.JavaLevel): Cr<hf.Level> {
  const specialSlots: Cr<Vector2D[]> = crOf([...javaLevel.specialSlots]);
  const scoreSlots: Cr<Vector2D[]> = crOf([...javaLevel.scoreSlots]);

  const rawScript: java.Script = javaLevel.script;
  let script: Cr<unknown>; // string | ParsedBetterScript
  switch (rawScript.type) {
    case "better":
      script = convertBetterScript(cx, rawScript.text);
      break;
    case "xml":
      script = crOf(rawScript.text);
      break;
    default:
      script = unreachable(rawScript.type, "Unexpected Script.type");
  }

  const skinHorizontalTiles: Cr<number> = convertJavaAssetId("tile", javaLevel.skins.horizontalTiles, cx.assets.tiles);
  const skinVerticalTiles: Cr<number> = convertJavaAssetId("tile", javaLevel.skins.verticalTiles, cx.assets.tiles);

  const badListCr: Cr<hf.BadSpawn>[] = [];
  for (const javaBad of javaLevel.bads) {
    badListCr.push(
      convertJavaAssetId("bad", javaBad.id, cx.assets.bads)
        .map(id => ({ id, x: javaBad.x, y: javaBad.y })),
    );
  }
  const badList: Cr<hf.BadSpawn[]> = crJoinArray(badListCr);

  const mapDiags: Diagnostic[] = [];
  const mapRaw: number[][] = [];
  for (const javaColumn of javaLevel.map) {
    const column: number[] = [];
    for (const javaCell of javaColumn) {
      switch (javaCell) {
        case "0":
          column.push(0);
          break;
        case "1":
          column.push(1);
          break;
        default: {
          const field: Cr<number> = convertFieldId(javaCell, cx.assets.fields, javaLevel.fields);
          column.push(-field.unwrapOr(0));
          for (const diag of field.diagnostics) {
            mapDiags.push(diag);
          }
          break;
        }
      }
    }
    mapRaw.push(column);
  }
  const map: Cr<number[][]> = new Cr(mapRaw, mapDiags);

  const skinBg: Cr<number> = convertJavaAssetId("background", javaLevel.skins.background, cx.assets.backgrounds);

  return crJoin({
    specialSlots,
    scoreSlots,
    skinBg,
    skinTiles: crJoinArray([skinHorizontalTiles, skinVerticalTiles]).map(skins => {
      if (skins[0] !== skins[1]) {
        return skins[0] + 100 * skins[1];
      }
      return skins[0];
    }),
    badList,
    script,
    map,
    playerX: crOf(javaLevel.playerSlot.x),
    playerY: crOf(javaLevel.playerSlot.y),
  });
}

function convertJavaAssetId(type: diags.AssetType, id: string, assets: ReadonlyMap<string, JavaAssetRef>): Cr<number> {
  const asset: JavaAsset | undefined = assets.get(id)?.ref;
  if (asset === undefined) {
    return crError(diags.assetNotFound(type, id));
  } else if (asset.value === undefined) {
    return crError(diags.missingAssetValue(type, id));
  } else {
    return crOf(asset.value);
  }
}

function convertFieldId(
  javaFieldId: string,
  javaFieldAssets: ReadonlyMap<string, JavaAssetRef>,
  fieldMap: ReadonlyArray<string>,
): Cr<number> {
  const fieldIndex: number | undefined = java.getFieldIndex(javaFieldId);
  if (fieldIndex === undefined) {
    return crError(diags.error(`InvalidFieldId: ${javaFieldId}`));
  }
  const fieldId: string | undefined = fieldMap[fieldIndex];
  if (fieldId === undefined) {
    return crError(diags.error(`FieldNotFound: javaFieldId = ${javaFieldId}`));
  }

  return convertJavaAssetId("ray", fieldId, javaFieldAssets);
}

function convertBetterScript(cx: CompilerContext, sourceText: string): Cr<unknown> {
  const errors: Diagnostic[] = [];
  function errorSink(e: BsScriptError) {
    errors.push({ ...diags.betterScriptError(e), sourceText, subfile: "script" });
  }

  const compiled: unknown = bs.compileScript(sourceText, errorSink, cx.resolver);
  return new Cr(compiled, errors);
}
