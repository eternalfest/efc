import { Cheerio, CheerioAPI, load as cheerioLoad } from "cheerio";
import { AnyNode, ChildNode, Element, Text } from "domhandler";

export function cheerioToString(root: Cheerio<Element>, pretty: boolean = false): string {
  const separator: string = pretty ? "\n" : "";
  const $: CheerioAPI = cheerioLoad(`<?xml version="1.0" encoding="UTF-8"?>${separator}`, {xmlMode: true});
  $.root().append(root);
  removeCheerioWhitespace($, root[0]);
  if (pretty) {
    indentCheerio($, root, 0);
  }
  return `${$.xml()}${separator}`;
}

function removeCheerioWhitespace($: CheerioAPI, node: Element): void {
  if (node.children === undefined) {
    return;
  }
  if (node.children.length >= 2) {
    const whitespace: ChildNode[] = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i: number = 0; i < node.children.length; i++) {
      const child: ChildNode = node.children[i];
      if (isWhitespace(child)) {
        whitespace.push(child);
      }
    }
    $(whitespace).remove();
  }
  // tslint:disable-next-line:prefer-for-of
  for (let i: number = 0; i < node.children.length; i++) {
    const child: Element = node.children[i] as any;
    removeCheerioWhitespace($, child);
  }
}

function isWhitespace(elem: AnyNode): boolean {
  return elem.type === "comment" || elem.type === "text" && elem.data !== undefined && /^\s+$/.test(elem.data);
}

function indentCheerio($: CheerioAPI, node: Cheerio<Element>, depth: number): void {
  if (depth > 10) {
    return;
  }
  node.children().before(createWhitespace($, depth + 1));
  node.children().last().after(createWhitespace($, depth));
  node.children().each((_, child) => {
    indentCheerio($, $(child), depth + 1);
  });
}

function createWhitespace($: CheerioAPI, depth: number): Cheerio<Text> {
  const wrapper: Cheerio<Element> = $<Element, string>("<div></div>");
  wrapper.text(`\n${new Array(depth).fill("  ").join("")}`);
  return wrapper.contents() as Cheerio<Text>;
}
