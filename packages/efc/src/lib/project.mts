import { Cheerio, CheerioAPI, SelectorType } from "cheerio";
import { Element } from "domhandler";
import { fromSysPath, join as furiJoin } from "furi";
import { combineLatest, firstValueFrom, Observable } from "rxjs";
import { map as rxMap, switchMap as rxSwitchMap } from "rxjs/operators";
import url, { URL } from "url";

import { loadXml } from "./cheerio-reader.mjs";
import { generateWays as chunkGenerateWays } from "./chunk.mjs";
import { CompilerContext, createCompilerContext } from "./compiler/context.mjs";
import { compileLevelSets, getJavaLevelSets, LevelSet } from "./compiler/level-set.mjs";
import { LevelPort, LevelRefMap } from "./compiler/links.mjs";
import { ObfFn, resolveObf } from "./compiler/obf.mjs";
import {
  CompilerResult as Cr,
  flattenRx as crFlattenRx,
  join as crJoin,
  joinArray as crJoinArray,
  joinMap as crJoinMap,
  of as crOf,
  warn as crWarn,
} from "./compiler-result.mjs";
import * as dataFiles from "./data-files.mjs";
import * as diags from "./diagnostic/errors.mjs";
import { GameData, Links, OutLevelSet, stringifyGameData, stringifyLocale } from "./game-data.mjs";
import { fromConfig as assetsFromConfig } from "./java/assets.mjs";
import {
  EfcAssetsConfig,
  EfcConfig,
  EfcLocaleConfig,
  fromPath as javaConfigFromPath,
  ResolvedConfig,
} from "./java/config.mjs";
import { DimensionsConfig, resolveDimensions, Way } from "./java/dimensions.mjs";
import {
  fromPath as javaDimensionsLocaleFromPath,
  JavaDimensionsLocale,
} from "./java/dimensions-locale.mjs";
import { fromJavaDimensions } from "./locales/lands-locale.mjs";
import { Locale } from "./locales/locale.mjs";
import { withTextFile } from "./utils.mjs";

export interface Compiled {
  content: string;
  locales: Map<string, string>;
}

export function compile(config: EfcConfig): Observable<Cr<Compiled>> {
  const dimensions$: Observable<Cr<DimensionsConfig>> = resolveDimensions(config.dimensions, config.levels);
  const obfFn$: Observable<Cr<ObfFn>> = resolveObf(config.obf);

  return combineLatest([dimensions$, obfFn$])
    .pipe(rxMap(([cr0, cr1]) => crJoinArray([cr0, cr1])))
    .pipe(rxSwitchMap(async (cr: Cr<[DimensionsConfig, ObfFn]>): Promise<Cr<Compiled>> => {
      return cr.flatMapAsync(async ([dimensions, obfFn]: [DimensionsConfig, ObfFn]): Promise<Cr<Compiled>> => {
        return (await assetsFromConfig(config.assets)).flatMapAsync(assets => {
          const cx: CompilerContext = createCompilerContext(assets, obfFn);
          return innerCompileFromDirectory(cx, config, dimensions);
        });
      });
    }));
}

export async function compileFromDirectory(
  dir: url.URL,
  prettyOutput: boolean = false,
  separateLocales: boolean = false,
): Promise<Cr<Compiled>> {
  const compiled$ = observeCompilationFromDirectory(dir, prettyOutput, separateLocales);
  return firstValueFrom(compiled$);
}

export function observeCompilationFromDirectory(
  dir: url.URL,
  prettyOutput: boolean = false,
  separateLocales: boolean = false,
): Observable<Cr<Compiled>> {
  const javaConfigPath: url.URL = furiJoin(dir, ["config.xml"]);
  return javaConfigFromPath(javaConfigPath).pipe(
    rxSwitchMap((javaConfig: Cr<ResolvedConfig>): Observable<Cr<Compiled>> => {
      return crFlattenRx(
        javaConfig.map(config => compileFromJavaConfig(dir, config, prettyOutput, separateLocales)),
      );
    }),
  );
}

function compileFromJavaConfig(
  dir: url.URL,
  javaConfig: ResolvedConfig,
  prettyOutput: boolean,
  separateLocales: boolean,
): Observable<Cr<Compiled>> {
  const locales: Map<string, EfcLocaleConfig> = new Map();
  for (const [localeId, javaLocaleConfig] of javaConfig.locales) {
    const efcLocaleConfig: EfcLocaleConfig = {
      items: fromSysPath(javaLocaleConfig.items),
      keys: fromSysPath(javaLocaleConfig.keys),
      lands: fromSysPath(javaLocaleConfig.lands),
      statics: fromSysPath(javaLocaleConfig.statics),
    };
    locales.set(localeId, efcLocaleConfig);
  }

  const assets: EfcAssetsConfig = {
    useBaseAssets: javaConfig.assets.useBaseAssets,
    roots: javaConfig.assets.roots.map(fromSysPath),
  };

  const config: EfcConfig = {
    root: dir,
    levels: fromSysPath(javaConfig.levelsDir),
    patcherData: fromSysPath(javaConfig.patcherDataDir),
    scoreItems: fromSysPath(javaConfig.scoreItemsFile),
    specialItems: fromSysPath(javaConfig.specialItemsFile),
    dimensions: fromSysPath(javaConfig.dimensionsFile),
    locales,
    assets,
    prettyOutput,
    separateLocales,
  };

  return compile(config);
}

async function innerCompileFromDirectory(
  cx: CompilerContext,
  config: EfcConfig,
  javaDimensions: DimensionsConfig,
): Promise<Cr<Compiled>> {
  return (await getJavaLevelSets(javaDimensions))
    .flatMapAsync(async (javaLevelSets: ReadonlyMap<string, LevelSet>): Promise<Cr<Compiled>> => {
      const levelSets: Cr<ReadonlyMap<string, OutLevelSet>> = compileLevelSets(cx, javaLevelSets);
      const patcherData: Cr<ReadonlyMap<string, unknown>> = crJoinMap(await dataFiles.get(cx, config.patcherData));
      const specialItems: Cr<Cheerio<Element>> = await readXml(config.specialItems, "items");
      const scoreItems: Cr<Cheerio<Element>> = await readXml(config.scoreItems, "items");

      return LevelRefMap.make(javaLevelSets, javaDimensions.tags).flatMapAsync(async levelRefMap => {
        const links: Cr<Links> = desugarWays(generateWays(javaLevelSets).concat(javaDimensions.ways), levelRefMap)
          .map(ways => ({ways, tags: levelRefMap.tags}));

        const localesMap: Map<string, Cr<Locale>> = new Map();
        for (const [localeId, localeConfig] of config.locales) {
          const javaDimensionsLocale: Cr<JavaDimensionsLocale> = await javaDimensionsLocaleFromPath(localeConfig.lands);
          localesMap.set(
            localeId,
            crJoin({
              statics: await readXml(localeConfig.statics, "statics"),
              items: await readXml(localeConfig.items, "items"),
              lands: javaDimensionsLocale.flatMap(dim => fromJavaDimensions(dim, levelRefMap)),
              keys: await readXml(localeConfig.keys, "keys"),
              quests: crOf(undefined),
              families: crOf(undefined),
            }),
          );
        }
        const outLocales: Map<string, string> = new Map();
        const inlineLocales: Cr<Map<string, Locale>> = crJoinMap(localesMap).map(locales => {
          if (config.separateLocales ?? false) {
            const prettyOutput: boolean = config.prettyOutput ?? false;
            for (const [localeId, locale] of locales) {
              outLocales.set(localeId, stringifyLocale(localeId, locale, prettyOutput));
            }
            locales.clear();
          }
          return locales;
        });

        return crJoin({
          levelSets,
          patcherData,
          links,
          specialItems,
          scoreItems,
          locales: inlineLocales,
        }).map((gameData: GameData) => ({
          content: stringifyGameData(gameData, config.prettyOutput ?? false),
          locales: outLocales,
        }));
      });

    });
}

function desugarWays(ways: ReadonlyArray<Way>, levelRefMap: LevelRefMap): Cr<ReadonlyArray<Way>> {
  const waysByFrom: Map<string, Way> = new Map();

  const waysCr: Cr<Way>[] = ways.map(way => {
    const type: Cr<"one-way" | "two-way"> = crOf(way.type);
    const from: Cr<LevelPort> = levelRefMap.desugarWithPort(way.from).flatMap(from => {
      const lvlPos = levelRefMap.resolve(from.level).unwrap();
      const port: number = from.port ?? 0;
      const key: string = `${lvlPos.did}:${lvlPos.levelId}:${port}}`;

      const duplicate: Way | undefined = waysByFrom.get(key);
      waysByFrom.set(key, way);

      if (duplicate === undefined) {
        return crOf(from);
      } else {
        return crWarn(from, diags.warn(`Duplicate link at did=${lvlPos.did}, lid=${lvlPos.levelId}, pid=${port}`));
      }
    });
    const to: Cr<LevelPort> = levelRefMap.desugarWithPort(way.to);
    return crJoin({type, from, to});
  });

  return crJoinArray(waysCr);
}

function generateWays(javaLevelSets: ReadonlyMap<string, LevelSet>): ReadonlyArray<Way> {
  const ways: ReadonlyArray<Way>[] = [];
  for (const levelSet of javaLevelSets.values()) {
    for (const chunk of levelSet.chunks) {
      const cur: ReadonlyArray<Way> = chunkGenerateWays(chunk);
      if (cur.length > 0) {
        ways.push(cur);
      }
    }
  }

  return ([] as Way[]).concat(...ways);
}

async function readXml(path: URL, rootName: string): Promise<Cr<Cheerio<Element>>> {
  return withTextFile(path, text => {
    const $: CheerioAPI = loadXml(text, true);
    return crOf($(rootName as SelectorType));
  });
}
