import { ScriptError } from "@eternalfest/better-scripts/error";
import { Cheerio } from "cheerio";
import { AnyNode, Element, ParentNode } from "domhandler";

import {
  ANY_ERROR,
  ANY_WARNING,
  ASSET_NOT_FOUND,
  BETTER_SCRIPT_ERROR,
  DUPLICATE_ASSET_ID,
  FS_IO_ERROR,
  MALFORMED_JSON,
  MALFORMED_JSON5,
  MALFORMED_TOML,
  MISSING_ASSET_VALUE,
  UNKNOWN_SCRIPT_TYPE_ERROR,
  XML_CHILD_NON_UNIQUE,
  XML_CHILD_NOT_FOUND,
  XML_INVALID_ATTRIBUTE,
  XML_MISSING_ATTRIBUTE,
} from "./codes.mjs";
import { Diagnostic, DiagnosticCategory, DiagnosticErr, DiagnosticWarn } from "./report.mjs";

type Writable<T> = { -readonly [P in keyof T ]: T[P] };

export function error(message: string): Writable<DiagnosticErr> {
  return {
    code: ANY_ERROR,
    category: DiagnosticCategory.Error,
    message,
  };
}

export function warn(message: string): Writable<DiagnosticWarn> {
  return {
    code: ANY_WARNING,
    category: DiagnosticCategory.Warning,
    message,
  };
}

export interface UnknownScriptType extends DiagnosticErr<typeof UNKNOWN_SCRIPT_TYPE_ERROR> {
  scriptType: string;
}

export function unknownScriptType(scriptType: string): UnknownScriptType {
  return {
    code: UNKNOWN_SCRIPT_TYPE_ERROR,
    category: DiagnosticCategory.Error,
    message: `Unexpected script type: ${scriptType}`,
    scriptType: scriptType,
  };
}

export interface BetterScriptError extends DiagnosticErr<typeof BETTER_SCRIPT_ERROR> {
  cause: ScriptError;
}

export function betterScriptError(cause: ScriptError): BetterScriptError {
  const error: Writable<BetterScriptError> = {
    code: BETTER_SCRIPT_ERROR,
    category: DiagnosticCategory.Error,
    message: cause.msg,
    cause,
  };

  if (cause.spans.length > 0) {
    // We can only have one span, so use the 'primary' one.
    const start: number = cause.spans[0].start;
    error.startOffset = start;
    error.length = cause.spans[0].end - start;
  }

  return error;
}

export interface FsIoError extends DiagnosticErr<typeof FS_IO_ERROR> {
  cause: NodeJS.ErrnoException;
}

export function fsIoError(cause: NodeJS.ErrnoException): FsIoError {
  // Note: **do not** set `fileUri`, for two reasons:
  // - the cause's message already include the offending path
  // - `fileUri` represents the file which triggered the error,
  // not the file we've failed to manipulate.
  return {
    code: FS_IO_ERROR,
    category: DiagnosticCategory.Error,
    message: cause.message,
    cause,
  };
}

export function malformedJson(cause: SyntaxError): DiagnosticErr<typeof MALFORMED_JSON> {
  const err: Writable<DiagnosticErr<typeof MALFORMED_JSON>> = {
    code: MALFORMED_JSON,
    category: DiagnosticCategory.Error,
    message: String(cause).replace("SyntaxError: ", ""),
  };

  // Try to recover a character offset (this is very hacky).
  const spaceIdx: number = err.message.lastIndexOf(" ");
  if (spaceIdx >= 0) {
    const num = parseInt(err.message.substring(spaceIdx + 1));
    if (!isNaN(num)) {
      err.startOffset = num;
    }
  }

  return err;
}

export interface JSON5SyntaxError extends SyntaxError {
  lineNumber: number;
  columnNumber: number;
}

export function malformedJson5(cause: JSON5SyntaxError, text: string): DiagnosticErr<typeof MALFORMED_JSON5> {
  // Translate 1-based line/col to character offset.
  let line = cause.lineNumber;
  let pos = -1;
  while (line > 1) {
    pos = text.indexOf("\n", pos + 1);
    line -= 1;
    if (pos < 0) {
      break;
    }
  }
  const startOffset = Math.max(0, pos + Math.max(0, cause.columnNumber));

  return {
    code: MALFORMED_JSON5,
    category: DiagnosticCategory.Error,
    message: String(cause).replace("SyntaxError: JSON5", ""),
    startOffset,
  };
}

export interface TOMLSyntaxError extends SyntaxError {
  offset?: number;
}

export function malformedToml(cause: TOMLSyntaxError): DiagnosticErr<typeof MALFORMED_TOML> {
  return {
    code: MALFORMED_TOML,
    category: DiagnosticCategory.Error,
    message: String(cause).replace("SyntaxError: ", ""),
    startOffset: cause.offset,
  };
}

interface Offsets {
  readonly startOffset?: number;
  readonly length?: number;
}

function getXmlElementOffsets(elem: AnyNode | undefined): Offsets {
  const startIndex: number | null | undefined = elem?.startIndex;
  const endIndex: number | null | undefined = elem?.endIndex;
  if (typeof startIndex === "number") {
    if (typeof endIndex === "number") {
      return {
        startOffset: Math.min(startIndex, endIndex),
        length: Math.abs(startIndex - endIndex),
      };
    } else {
      return {startOffset: startIndex};
    }
  } else {
    return (typeof endIndex === "number") ? {startOffset: endIndex} : {};
  }
}

export interface XmlChildNotFound extends Diagnostic<typeof XML_CHILD_NOT_FOUND> {
  readonly selector: string;
}

export function xmlChildNotFound(
  parent: Cheerio<ParentNode>,
  selector: string,
  category: DiagnosticCategory = DiagnosticCategory.Error,
): XmlChildNotFound {
  const message: string = `XML child not found for selector ${JSON.stringify(selector)}`;
  const {startOffset, length} = getXmlElementOffsets(parent[0]);
  return {
    code: XML_CHILD_NOT_FOUND,
    category,
    message,
    startOffset,
    length,
    selector,
  };
}

export interface XmlChildNonUnique extends DiagnosticErr<typeof XML_CHILD_NON_UNIQUE> {
  readonly selector: string;
}

export function xmlChildNonUnique(duplicate: Element, selector: string): XmlChildNonUnique {
  const message: string = `XML child must be unique for selector ${JSON.stringify(selector)}`;
  const {startOffset, length} = getXmlElementOffsets(duplicate);
  return {
    code: XML_CHILD_NON_UNIQUE,
    category: DiagnosticCategory.Error,
    message,
    startOffset,
    length,
    selector,
  };
}

export interface XmlMissingAttribute extends DiagnosticErr<typeof XML_MISSING_ATTRIBUTE> {
  readonly attribute: string;
}

export function xmlMissingAttribute(node: Cheerio<Element>, attribute: string): XmlMissingAttribute {
  const message: string = `Missing XML attribute ${JSON.stringify(attribute)}`;
  const {startOffset, length} = getXmlElementOffsets(node[0]);
  return {
    code: XML_MISSING_ATTRIBUTE,
    category: DiagnosticCategory.Error,
    message,
    startOffset,
    length,
    attribute,
  };
}

export interface XmlInvalidAttribute extends DiagnosticErr<typeof XML_INVALID_ATTRIBUTE> {
  readonly attribute: string;
  readonly value: string;
}


export function xmlInvalidAttribute(node: Cheerio<Element>, attribute: string, value: string): XmlInvalidAttribute {
  const message: string = `Invalid XML attribute ${JSON.stringify(attribute)}: ${JSON.stringify(value)}`;
  const {startOffset, length} = getXmlElementOffsets(node[0]);
  return {
    code: XML_INVALID_ATTRIBUTE,
    category: DiagnosticCategory.Error,
    message,
    startOffset,
    length,
    attribute,
    value,
  };
}

export type AssetType = "background" | "tile" | "ray" | "bad";

export interface AssetNotFound extends DiagnosticErr<typeof ASSET_NOT_FOUND> {
  readonly type: AssetType;
  readonly key: string;
}

export function assetNotFound(type: AssetType, key: string): AssetNotFound {
  const message: string = `Unknown ${type} asset ${JSON.stringify(key)}`;
  return {
    code: ASSET_NOT_FOUND,
    category: DiagnosticCategory.Error,
    message,
    type,
    key,
  };
}

export interface MissingAssetValue extends DiagnosticErr<typeof MISSING_ASSET_VALUE> {
  readonly type: AssetType;
  readonly key: string;
}

export function missingAssetValue(type: AssetType, key: string): MissingAssetValue {
  const message: string = `Missing value for ${type} asset ${JSON.stringify(key)}`;
  return {
    code: MISSING_ASSET_VALUE,
    category: DiagnosticCategory.Error,
    message,
    type,
    key,
  };
}

export interface DuplicateAssetId extends DiagnosticErr<typeof DUPLICATE_ASSET_ID> {
  readonly type: AssetType;
  readonly key: string;
}

export function duplicateAssetId(type: AssetType, key: string): DuplicateAssetId {
  const message: string = `Dupplicate ${type} asset for id ${JSON.stringify(key)}`;
  return {
    code: DUPLICATE_ASSET_ID,
    category: DiagnosticCategory.Error,
    message,
    type,
    key,
  };
}
