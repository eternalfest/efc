import { Cheerio, CheerioAPI } from "cheerio";
import { Element } from "domhandler";

import { loadXml, readAttr } from "../cheerio-reader.mjs";
import { LevelRefPos, parseLevelRefExpr } from "../compiler/links.mjs";
import {
  CompilerResult as Cr,
  joinArray as crJoinArray,
  joinMapEntries as crJoinMapEntries,
} from "../compiler-result.mjs";
import { withTextFile } from "../utils.mjs";

export type JavaDimensionsLocale = ReadonlyMap<LevelRefPos, string>;

export async function fromPath(configUrl: URL): Promise<Cr<JavaDimensionsLocale>> {
  return withTextFile(configUrl, fromString);
}

export function fromString(text: string): Cr<ReadonlyMap<LevelRefPos, string>> {
  const $: CheerioAPI = loadXml(text, true);
  return readDimensionsLocale($, $("dimensions"));
}

function readDimensionsLocale($: CheerioAPI, node: Cheerio<Element>): Cr<ReadonlyMap<LevelRefPos, string>> {
  const levels: Cr<[LevelRefPos, string]>[] = [];

  node
    .children("dimension")
    .each((_: number, elem: Element): void => {
      const setNode: Cheerio<Element> = $(elem);
      readAttr(setNode, "id").map(did => {
        setNode
          .children("level")
          .each((_: number, elem: Element): void => {
            levels.push(readLevelLocale($(elem)).map(lvlLocale => {
              const [key, text] = lvlLocale;
              const levelId: number = parseInt(key, 10);
              return [{refType: "pos", did, levelId}, text];
            }));
          });
      });
    });

  node
    .children("level")
    .each((_: number, elem: Element): void => {
      levels.push(readLevelLocale($(elem)).flatMap(lvlLocale => {
        const [key, text] = lvlLocale;
        return parseLevelRefExpr(key).map(key => [key, text]);
      }));
    });

  node
    .children("contree")
    .each((_: number, elem: Element): void => {
      levels.push(readLevelLocale($(elem)).map(lvlLocale => {
        const [key, text] = lvlLocale;
        return [{refType: "tag", tag: key}, text];
      }));
    });

  return crJoinMapEntries(levels);
}

function readLevelLocale(node: Cheerio<Element>): Cr<[string, string]> {
  const key: Cr<string> = readAttr(node, "id");
  const text: Cr<string> = readAttr(node, "name");
  return crJoinArray([key, text]);
}
