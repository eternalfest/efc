import { Cheerio, CheerioAPI} from "cheerio";
import { Element } from "domhandler";

import { cheerioMapChildren, loadXml, readAttr, readFloatAttr, readOneChild, readOneOrNoneChild } from "../cheerio-reader.mjs";
import { Vector2D } from "../compiler/vector-2d.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  join as crJoin,
  joinArray as crJoinArray,
  of as crOf,
  warn as crWarn,
} from "../compiler-result.mjs";
import * as diags from "../diagnostic/errors.mjs";
import { DiagnosticCategory } from "../diagnostic/report.mjs";
import { withTextFile } from "../utils.mjs";

export interface JavaLevel {
  readonly url?: URL;
  readonly source?: string;
  readonly version: string;
  readonly uid?: string;
  readonly name: string;
  readonly description: string;
  readonly script: Script;
  readonly skins: Skins;
  readonly fields: ReadonlyArray<string>;
  // map[x][y]
  readonly map: ReadonlyArray<ReadonlyArray<string>>;
  readonly playerSlot: Vector2D;
  readonly specialSlots: ReadonlyArray<Vector2D>;
  readonly scoreSlots: ReadonlyArray<Vector2D>;
  readonly bads: ReadonlyArray<Bad>;
}

export type Script = {
  readonly type: "xml" | "better";
  readonly text: string;
}

export interface Skins {
  readonly background: string;
  readonly horizontalTiles: string;
  readonly verticalTiles: string;
}

export interface Bad {
  readonly id: string;
  readonly x: number;
  readonly y: number;
}

export const HF_LEVEL_WIDTH: number = 20;
export const HF_LEVEL_HEIGHT: number = 25;
const DEFAULT_PLAYER_SLOT: Vector2D = {x: 0, y: 0};

export function fromFile(fileUri: URL): Promise<Cr<JavaLevel>> {
  return withTextFile(fileUri, text => {
    const $: CheerioAPI = loadXml(text);
    return readLevel($, $("level"), text, fileUri);
  });
}

export function fromString(text: string): Cr<JavaLevel> {
  const $: CheerioAPI = loadXml(text);
  return readLevel($, $("level"), text)
    .mapDiagnostics(d => ({ ...d, sourceText: d.sourceText ?? text }));
}

const UPPER_CASE_A: number = "A".codePointAt(0)!;
export function getFieldIndex(fieldChar: string): number | undefined {
  const cp: number = fieldChar.codePointAt(0)!;
  return cp < UPPER_CASE_A ? undefined : cp - UPPER_CASE_A;
}

function readLevel($: CheerioAPI, node: Cheerio<Element>, source?: string, url?: URL): Cr<JavaLevel> {
  return crJoin({
    url: crOf(url),
    source: crOf(source),
    version: readAttr(node, "version"),
    uid: crOf(node.attr("uid")),
    name: readOneChild(node, "name").map(node => node.text()),
    description: readOneChild(node, "name").map(node => node.text()),
    script: readOneOrNoneChild(node, "script")
      .flatMap(child => child !== undefined ? readScript(child) : crOf(getDefaultScript())),
    skins: readOneChild(node, "skins").flatMap(readSkins),
    fields: readOneOrNoneChild(node, "rayons")
      .flatMap(child => child !== undefined ? readFields($, child) : crOf(getDefaultFields())),
    map: readOneChild(node, "plateformes").flatMap(readTileMap),
    playerSlot: readOneOrNoneChild(node, "player")
      .flatMap(child => child !== undefined
        ? readVector2D(child)
        : crWarn(DEFAULT_PLAYER_SLOT, diags.xmlChildNotFound(node, "player", DiagnosticCategory.Warning)),
      ),
    specialSlots: crJoinArray(cheerioMapChildren($, node, "effet", readVector2D)),
    scoreSlots: crJoinArray(cheerioMapChildren($, node, "points", readVector2D)),
    bads: readOneOrNoneChild(node, "badlist")
      .flatMap(child => child !== undefined ? readBadList($, child) : crOf([])),
  });
}

function readScript(node: Cheerio<Element>): Cr<Script> {
  const type: string | undefined = node.attr("type");
  // We must explicitly get the child text nodes because `cheerio` ignore
  // text in `script` tags.
  const text: string = node.contents().text();
  switch (type) {
    case "new": {
      return crOf({ type: "better", text });
    }
    case undefined:
    case "xml": {
      return crOf({ type: "xml", text });
    }
    default: {
      if (node.contents().length === 0) {
        return crOf(getDefaultScript());
      } else {
        return crError(diags.unknownScriptType(type));
      }
    }
  }
}

function getDefaultScript(): Script {
  return {type: "xml", text: ""};
}

function readSkins(node: Cheerio<Element>): Cr<Skins> {
  return crJoin({
    background: readOneChild(node, "fond").flatMap(node => readAttr(node, "id")),
    horizontalTiles: readOneChild(node, "plateformeH").flatMap(node => readAttr(node, "id")),
    verticalTiles: readOneChild(node, "plateformeV").flatMap(node => readAttr(node, "id")),
  });
}

function readFields($: CheerioAPI, node: Cheerio<Element>): Cr<string[]> {
  return crJoinArray(cheerioMapChildren($, node, "r", child => readAttr(child, "name")))
    .map(fields => fields[0] === "none" ? fields.slice(1) : fields);
}

function getDefaultFields(): string[] {
  return [
    "blanc",
    "noir",
    "bleu",
    "vert",
    "rouge",
    "teleport",
    "vortex",
    "soccer_violet",
    "soccer_jaune",
    "J-O",
    "nobombs",
  ];
}

function readTileMap(node: Cheerio<Element>): Cr<string[][]> {
  const HF_LEVEL_CELL_COUNT: number = HF_LEVEL_WIDTH * HF_LEVEL_HEIGHT;
  const str: string = node.text().trim();
  if (str.length !== HF_LEVEL_CELL_COUNT) {
    return crError(diags.error(`Invalid map length, actual: ${str.length}, expected: ${HF_LEVEL_CELL_COUNT}`));
  }
  const map: string[][] = [];
  let nextColumn: string[] = [];
  for (const chr of str) {
    nextColumn.push(chr);
    if (nextColumn.length === HF_LEVEL_HEIGHT) {
      map.push(nextColumn);
      nextColumn = [];
    }
  }
  return crOf(map);
}

function readVector2D(node: Cheerio<Element>): Cr<Vector2D> {
  return crJoin({
    x: readFloatAttr(node, "x"),
    y: readFloatAttr(node, "y"),
  });
}

function readBadList($: CheerioAPI, node: Cheerio<Element>): Cr<Bad[]> {
  return crJoinArray(cheerioMapChildren($, node, "bad", readBad));
}

function readBad(node: Cheerio<Element>): Cr<Bad> {
  return crJoin({
    id: readAttr(node, "id"),
    x: readFloatAttr(node, "x"),
    y: readFloatAttr(node, "y"),
  });
}
