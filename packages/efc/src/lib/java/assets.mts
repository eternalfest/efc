import path from "node:path";
import { fileURLToPath } from "node:url";

import { Cheerio, CheerioAPI } from "cheerio";
import { Element } from "domhandler";
import { fromSysPath, toSysPath } from "furi";

import { loadXml, readAttr, readAttrWithFallback, readOneChild, readOneChildWithFallback, readOneOrNoneChild } from "../cheerio-reader.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  errors as crErrors,
  hasValue as crHasValue,
  join as crJoin,
  joinArray as crJoinArray,
  joinMapEntries as crJoinMapEntries,
  of as crOf,
} from "../compiler-result.mjs";
import * as diags from "../diagnostic/errors.mjs";
import { Diagnostic } from "../diagnostic/report.mjs";
import { withTextFile, withTextFileOrDir } from "../utils.mjs";
import { EfcAssetsConfig } from "./config.mjs";

export interface JavaAssets {
  readonly fields: ReadonlyMap<string, JavaAssetRef>;
  readonly backgrounds: ReadonlyMap<string, JavaAssetRef>;
  readonly tiles: ReadonlyMap<string, JavaAssetRef>;
  readonly bads: ReadonlyMap<string, JavaAssetRef>;
}

export interface JavaAssetRef {
  ref: JavaAsset;
  hidden: boolean;
}

export interface JavaAsset {
  readonly id: string;
  readonly name: string;
  readonly value?: number;
}

export const BASE_ASSETS_DIR: URL = fromSysPath(path.resolve(fileURLToPath(import.meta.url), "..", "..", "..", "assets", "assets.xml"));

export function fromConfig(config: EfcAssetsConfig): Promise<Cr<JavaAssets>> {
  if (config.useBaseAssets) {
    return fromDirectories(BASE_ASSETS_DIR, ...config.roots);
  } else {
    return fromDirectories(...config.roots);
  }
}

export async function fromDirectories(...roots: URL[]): Promise<Cr<JavaAssets>> {
  const assets: Cr<RawJavaAssets>[] = await Promise.all(roots.map(rawFromDirectory));
  return crJoinArray(assets).flatMap(mergeRawAssets);
}

interface AssetAlias {
  id: string;
  ref: string;
  hidden: boolean;
}

type RawAssetList = [ReadonlyMap<string, JavaAsset>, AssetAlias[]];

interface RawJavaAssets {
  readonly fields: RawAssetList;
  readonly backgrounds: RawAssetList;
  readonly tiles: RawAssetList;
  readonly bads: RawAssetList;
}

async function rawFromDirectory(rootUrl: URL): Promise<Cr<RawJavaAssets>> {
  return withTextFileOrDir(rootUrl, async text => {
    const rootPath: string = toSysPath(rootUrl);
    let $: CheerioAPI | null = null;
    if (text !== null) {
      $ = loadXml(text);
    }
    return crJoin({
      fields: await assetsFromXml($, rootPath, PARSE_FIELDS),
      backgrounds: await assetsFromXml($, rootPath, PARSE_BACKGROUNDS),
      tiles: await assetsFromXml($, rootPath, PARSE_TILES),
      bads: await assetsFromXml($, rootPath, PARSE_BADS),
    });
  });
}

function mergeRawAssets(raws: RawJavaAssets[]): Cr<JavaAssets> {
  const diagList: Diagnostic[] = [];

  function merge(raws: RawJavaAssets[], key: keyof RawJavaAssets, type: diags.AssetType): Map<string, JavaAssetRef> {
    const out: Map<string, JavaAssetRef> = new Map();

    // Merge assets definitions; later defitions can be shadowed.
    for (const { [key]: [assets, _] } of raws) {
      for (const [id, asset] of assets) {
        out.set(id, { ref: asset, hidden: false });
      }
    }

    // Add aliases; they **cannot** shadow other assets or aliases.
    for (const { [key]: [_, aliases] } of raws) {
      for (const alias of aliases) {
        const asset: JavaAssetRef | undefined = out.get(alias.ref);
        const shadowed: JavaAssetRef | undefined = out.get(alias.id);

        if (asset === undefined) {
          diagList.push(diags.assetNotFound(type, alias.ref));
        } else if (shadowed === undefined) {
          out.set(alias.id, { ref: asset.ref, hidden: alias.hidden });
        } else if (shadowed.ref === asset.ref) {
          out.set(alias.id, { ref: asset.ref, hidden: alias.hidden && shadowed.hidden });
        } else {
          diagList.push(diags.duplicateAssetId(type, alias.id));
        }
      }
    }

    return out;
  }

  const value: JavaAssets = {
    fields: merge(raws, "fields", "ray"),
    backgrounds: merge(raws, "backgrounds", "background"),
    tiles: merge(raws, "tiles", "tile"),
    bads: merge(raws, "bads", "bad"),
  };

  return new Cr(value, diagList);
}

interface ParseAssetsOptions {
  type: diags.AssetType;
  nodeName: string;
  elemName: string;
  legacyPath: string;
  legacyNodeName?: string;
  legacyElemName?: string;
}

const PARSE_BACKGROUNDS: ParseAssetsOptions = {
  type: "background",
  nodeName: "backgrounds",
  elemName: "type",
  legacyPath: "xml/fonds.xml",
  legacyNodeName: "fonds",
};

const PARSE_FIELDS: ParseAssetsOptions = {
  type: "ray",
  nodeName: "rays",
  elemName: "type",
  legacyPath: "xml/rayons.xml",
  legacyNodeName: "rayons",
};

const PARSE_TILES: ParseAssetsOptions = {
  type: "tile",
  nodeName: "tiles",
  elemName: "type",
  legacyPath: "xml/plateformes.xml",
  legacyNodeName: "plateformes",
};

const PARSE_BADS: ParseAssetsOptions = {
  type: "bad",
  nodeName: "sprites",
  elemName: "bad",
  legacyPath: "xml/sprites.xml",
  legacyElemName: "fruit",
};

async function assetsFromXml(
  $: CheerioAPI | null,
  rootPath: string,
  opts: ParseAssetsOptions
): Promise<Cr<RawAssetList>> {
  let nodeOrPath: Cheerio<Element> | string;
  if ($ !== null) {
    const node = readOneChild($.root(), "assets")
      .flatMap(n => readOneOrNoneChild(n, opts.nodeName));

    if (!crHasValue(node)) {
      return crErrors(node.diagnostics);
    } else if (node.value === undefined) {
      // No node in XML document: ignore.
      return crOf([new Map(), []]);
    }

    const attrPath = node.value.attr("path");
    if (attrPath === undefined) {
      // Nested mode: use node directly.
      nodeOrPath = node.value;
    } else {
      // Detached node: load XML from specified path.
      nodeOrPath = path.posix.join(path.posix.dirname(rootPath), attrPath);
    }
  } else {
    // Legacy mode: load XML from default path.
    nodeOrPath = path.posix.join(rootPath, opts.legacyPath);
  }

  if (typeof nodeOrPath === "string") {
    const path: URL = fromSysPath(nodeOrPath);
    const isLegacy = $ === null;
    return withTextFile(path, text => {
      const $: CheerioAPI = loadXml(text);
      // Only allow legacy root node name in "legacy mode".
      const fallback = (isLegacy ? opts.legacyNodeName : undefined) ?? opts.nodeName;
      return readOneChildWithFallback($.root(), opts.nodeName, fallback)
        .flatMap(node => assetsFromXmlNode($, node, opts));
    });
  } else {
    return assetsFromXmlNode($!, nodeOrPath, opts);
  }
}

function assetsFromXmlNode(
  $: CheerioAPI,
  node: Cheerio<Element>,
  opts: ParseAssetsOptions,
): Cr<RawAssetList> {

  const assetsList: Cr<[string, JavaAsset]>[] = [];
  const aliasesList: Cr<AssetAlias>[] = [];

  node
    .children()
    .each((_: number, elem: Element): void => {
      if (elem.type !== "tag") {
        return;
      }

      const node: Cheerio<Element> = $(elem);
      if (elem.name === "alias") {
        aliasesList.push(crJoin({
          id: readAttrWithFallback(node, "id", "ID"),
          ref: readAttr(node, "ref"),
          hidden: crOf(node.attr("hidden") !== undefined),
        }));
      } else if (elem.name === opts.elemName || elem.name === opts.legacyElemName) {
        const id: Cr<string> = readAttrWithFallback(node, "id", "ID");
        const name: Cr<string> = readAttr(node, "name");
        const valueStr: string | undefined = node.attr("value");

        let asset: Cr<JavaAsset>;
        if (valueStr === undefined) {
          asset = crJoin({id, name});
        } else {
          const valueNum = parseInt(valueStr, 10);
          const value: Cr<number> = isNaN(valueNum) ?
            crError(diags.xmlInvalidAttribute(node, "value", valueStr))
            : crOf(valueNum);
          asset = crJoin({id, name, value});
        }

        assetsList.push(asset.map(asset => [asset.id, asset]));
      }
    });

  const assets: Cr<Map<string, JavaAsset>> = crJoinMapEntries(
    assetsList,
    (key, _old, _new) => crError(diags.duplicateAssetId(opts.type, key))
  );
  const aliases: Cr<AssetAlias[]> = crJoinArray(aliasesList);
  return crJoinArray([assets, aliases]);
}
