import { Cheerio, CheerioAPI } from "cheerio";
import { Element } from "domhandler";
import { join as furiJoin, toSysPath } from "furi";
import * as sysPath from "path";
import { combineLatest, Observable, of as rxOf } from "rxjs";
import { map as rxMap } from "rxjs/operators";
import { URL } from "url";

import { loadXml, readAttr } from "../cheerio-reader.mjs";
import { LevelPort, LevelRef, LevelRefPos, parseLevelPortExpr, parseLevelRefExpr } from "../compiler/links.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  join as crJoin,
  joinArray as crJoinArray,
  joinMapEntries as crJoinMapEntries,
  of as crOf,
} from "../compiler-result.mjs";
import * as diags from "../diagnostic/errors.mjs";
import { observeTree, ReadonlyFsTree } from "../observable-fs.mjs";
import { withObservedTextFile } from "../utils.mjs";
import { EfcLevelSetConfig } from "./config.mjs";

/**
 * Represents the input config from `dimensions.xml`.
 */
export interface InputDimensionsConfig {
  /**
   * Map from level set key to the corresponding chunks
   */
  readonly chunks: ReadonlyMap<string, ReadonlyArray<InputChunkConfig>>;
  readonly tags: ReadonlyMap<string, LevelRefPos>;
  readonly ways: ReadonlyArray<Way>;
}

export interface InputChunkConfig {
  readonly type: "normal" | "grid";
  readonly name: string;
  readonly dir: string;
  readonly isClosed: boolean;
}

export interface InputChunkConfig {
  readonly type: "normal" | "grid";
  readonly name: string;
  readonly dir: string;
  readonly isClosed: boolean;
}

/**
 * Represents a fully resolved dimensions config
 *
 * After resolution, the absolute paths and metadata for each chunk are known.
 */
export interface DimensionsConfig {
  /**
   * Map from level set key to the corresponding chunks
   */
  readonly sets: ReadonlyMap<string, LevelSetConfig>;
  readonly tags: ReadonlyMap<string, LevelRefPos>;
  readonly ways: ReadonlyArray<Way>;
}

export interface LevelSetConfig {
  readonly varName: string;
  readonly did: string | null;
  readonly chunks: ReadonlyArray<ChunkConfig>;
  readonly hasPaddingLevel: boolean;
  readonly ensureEqualLengths: boolean;
}

export type ChunkConfig = GridChunkConfig | LinearChunkConfig | TransientChunkConfig;

export interface GridChunkConfig {
  readonly type: "grid";
  readonly name: string;
  readonly dir: URL;
}

export interface LinearChunkConfig {
  readonly type: "linear";
  readonly name: string;
  readonly dir: URL;
  readonly emitTag: boolean;
}

export interface TransientChunkConfig {
  readonly type: "transient";
  readonly children: ReadonlyArray<ChunkConfig>;
}

export interface Way {
  type: "one-way" | "two-way";
  from: LevelPort;
  to: LevelPort;
}

export function resolveDimensions(
  dimConfigUrl: URL,
  levels: URL | ReadonlyMap<string, EfcLevelSetConfig>,
): Observable<Cr<DimensionsConfig>> {
  const levelSetMap: Observable<Cr<ReadonlyMap<string, EfcLevelSetConfig>>> = getLevelSetMap(levels);
  const dimConfig: Observable<Cr<InputDimensionsConfig>> = getDimensionsConfig(dimConfigUrl);
  return combineLatest([levelSetMap, dimConfig])
    .pipe(rxMap(
      (crs: [Cr<ReadonlyMap<string, EfcLevelSetConfig>>, Cr<InputDimensionsConfig>]): Cr<DimensionsConfig> => {
        return crJoinArray(crs)
          .map((
            [levelSetMap, dimConfig]: [ReadonlyMap<string, EfcLevelSetConfig>, InputDimensionsConfig],
          ): DimensionsConfig => {
            return resolveDimensionsConfig(levelSetMap, dimConfig);
          });
      },
    ));
}

function getLevelSetMap(
  levels: URL | ReadonlyMap<string, EfcLevelSetConfig>,
): Observable<Cr<ReadonlyMap<string, EfcLevelSetConfig>>> {
  if (levels instanceof URL) {
    return observeTree("*/", {cwd: toSysPath(levels)})
      .pipe(rxMap((fsTree: ReadonlyFsTree): Cr<ReadonlyMap<string, EfcLevelSetConfig>> => {
        const fsTreeKey: string = sysPath.resolve(toSysPath(levels));
        const levelDirEntities: ReadonlyMap<string, number> | undefined = fsTree.directories.get(fsTreeKey);
        if (levelDirEntities === undefined) {
          return crError(diags.error(`Level directory not found: ${levels}`));
        }
        const sets: Map<string, EfcLevelSetConfig> = new Map();
        for (const entName of levelDirEntities.keys()) {
          let key: string;
          let varName: string;
          let equalSize: boolean = false;
          let padding: boolean = false;
          let did: string | undefined;
          switch (entName) {
            case "adv_puits":
              key = "0";
              did = "0";
              varName = "LVLS_MAIN";
              break;
            case "adv_dim1":
              key = "1";
              did = "1";
              varName = "LVLS_DIM1";
              padding = true;
              break;
            case "adv_dim2":
              key = "2";
              did = "2";
              varName = "LVLS_DIM2";
              padding = true;
              break;
            case "adv_dim3":
              key = "3";
              did = "3";
              varName = "LVLS_DIM3";
              padding = true;
              break;
            case "adv_dim4":
              key = "4";
              did = "4";
              varName = "LVLS_DIM4";
              padding = true;
              break;
            case "apprentissage":
              key = "shareware";
              varName = "LVLS_SHAREWARE";
              break;
            case "soccerfest":
              key = "soccer";
              varName = "LVLS_SOCCER";
              break;
            case "timeattack":
              key = "time";
              varName = "LVLS_TIME";
              equalSize = true;
              break;
            case "timeattack_multi":
              key = "multitime";
              varName = "LVLS_MULTI_TIME";
              equalSize = true;
              break;
            case "tutorial":
              key = "tutorial";
              varName = "LVLS_TUTO";
              break;
            case "fjv":
              key = "fjv";
              varName = "LVLS_FJV";
              break;
            case "hof":
              key = "hof";
              varName = "LVLS_HOF";
              break;
            case "multi":
              key = "multi";
              varName = "LVLS_UNK";
              break;
            case "test":
              key = "test";
              varName = "LVLS_TEST";
              break;
            case "dev":
              key = "dev";
              varName = "LVLS_DEV";
              break;
            default:
              continue;
          }
          const dir: URL = furiJoin(levels, entName);
          sets.set(key, {varName, equalSize, padding, did, dir});
        }
        return crOf(sets);
      }));
  } else {
    return rxOf(crOf(levels));
  }
}

function getDimensionsConfig(dimConfigUrl: URL): Observable<Cr<InputDimensionsConfig>> {
  return withObservedTextFile(dimConfigUrl, text => {
    const $: CheerioAPI = loadXml(text, true);
    return readDimensionsConfig($, $("dimensions"));
  });
}

function readDimensionsConfig($: CheerioAPI, node: Cheerio<Element>): Cr<InputDimensionsConfig> {
  const chunksIter: Cr<[string, InputChunkConfig[]]>[] = [];
  node
    .children("contrees").children("dimension")
    .each((_: number, elem: Element): void => {
      chunksIter.push(readLevelSetChunkConfig($, $(elem)));
    });

  const tagsIter: Cr<[string, LevelRefPos]>[] = [];
  node
    .children("tags").children("tag")
    .each((_: number, elem: Element): void => {
      tagsIter.push(readTag($(elem)));
    });

  return crJoin({
    chunks: crJoinMapEntries(
      chunksIter,
      (key, _old, _new) => crError(diags.error(`Duplicate chunk: ${key}`)),
    ),
    tags: crJoinMapEntries(
      tagsIter,
      (key, _old, _new) => crError(diags.error(`Duplicate tag: ${key}`)),
    ),
    ways: readWays($, node.children("ways")),
  });
}

function readLevelSetChunkConfig(
  $: CheerioAPI,
  node: Cheerio<Element>,
): Cr<[string, InputChunkConfig[]]> {
  const key: Cr<string> = readAttr(node, "id");
  const chunks: Cr<InputChunkConfig>[] = [];

  node
    .children("contree")
    .each((_: number, elem: Element): void => {
      chunks.push(readChunkConfig($(elem)));
    });

  return crJoinArray([key, crJoinArray(chunks)]);
}

function validateTagName(name: Cr<string>, kind: string): Cr<string> {
  return name.flatMap(name => {
    if (name.toLowerCase() !== name) {
      const msg: string = `Level ${kind} '${name}' should not contain uppercase letters`;
      return new Cr(name, [diags.warn(msg)]);
    } else {
      return crOf(name);
    }
  });
}

function readChunkConfig(node: Cheerio<Element>): Cr<InputChunkConfig> {
  const name: Cr<string> = validateTagName(readAttr(node, "name"), "chunk");
  const dir: Cr<string> = readAttr(node, "dir");
  let type: Cr<"normal" | "grid">;
  let isClosed: Cr<boolean> = crOf(true);
  const typeAttr: string | undefined = node.attr("type");
  switch (typeAttr) {
    case "normal":
    case undefined: {
      const closedAttr: string | undefined = node.attr("closed");
      isClosed = crOf(closedAttr === undefined ? true : closedAttr !== "false");
      type = crOf("normal");
      break;
    }
    case "grid": {
      type = crOf("grid");
      break;
    }
    default: {
      type = crError(diags.error(`Unexpected dimension chunk type: ${typeAttr}`));
      break;
    }
  }
  return crJoin({name, dir, type, isClosed});
}

function readTag(node: Cheerio<Element>): Cr<[string, LevelRefPos]> {
  const name: Cr<string> = validateTagName(readAttr(node, "name"), "tag");
  const ref: string | undefined = node.attr("ref");

  if (ref !== undefined) {
    const refExpr: Cr<LevelRef> = parseLevelRefExpr(ref);
    return crJoinArray([name, refExpr]);
  } else {
    const levelId: Cr<number> = readAttr(node, "lid").map(id => parseInt(id, 10));
    const did: Cr<string> = readAttr(node, "did");
    return crJoin({ name, levelId, did }).map(vals => [
      vals.name,
      {
        refType: "pos",
        levelId: vals.levelId,
        did: vals.did,
      },
    ]);
  }
}

function readWays($: CheerioAPI, node: Cheerio<Element>): Cr<Way[]> {
  const ways: Cr<Way>[] = [];
  node
    .children()
    .each((_: number, _elem: Element): void => {
      const elem: Element = _elem;
      const wayNode: Cheerio<Element> = $(elem);
      const from: Cr<LevelPort> = readAttr(wayNode, "from").flatMap(parseLevelPortExpr);
      const to: Cr<LevelPort> = readAttr(wayNode, "to").flatMap(parseLevelPortExpr);
      let type: Cr<"one-way" | "two-way">;
      switch (elem.tagName) {
        case "oneway":
          type = crOf("one-way");
          break;
        case "twoway":
          type = crOf("two-way");
          break;
        default:
          type = crError(diags.error(`Unknown way type: ${elem.tagName}`));
          break;
      }

      ways.push(crJoin({ type, from, to }));
    });
  return crJoinArray(ways);
}

function resolveDimensionsConfig(
  levelSetMap: ReadonlyMap<string, EfcLevelSetConfig>,
  dimConfig: InputDimensionsConfig,
): DimensionsConfig {
  const resolvedSets: Map<string, LevelSetConfig> = new Map();
  for (const [key, inputLevelSetConfig] of levelSetMap) {
    const did: string | null = inputLevelSetConfig.did !== undefined ? inputLevelSetConfig.did : null;
    const inputChunks: ReadonlyArray<InputChunkConfig> | undefined = dimConfig.chunks.get(key);
    const chunks: ChunkConfig[] = [];
    if (inputChunks === undefined) {
      chunks.push({
        type: "linear",
        dir: inputLevelSetConfig.dir,
        name: key,
        emitTag: false,
      });
    } else {
      let chain: ChunkConfig[] = [];
      for (const inputChunk of inputChunks) {
        const dir: URL = furiJoin(inputLevelSetConfig.dir, inputChunk.dir);
        const chunk: ChunkConfig = inputChunk.type === "normal"
          ? {type: "linear", name: inputChunk.name, dir, emitTag: did !== null}
          : {type: "grid", name: inputChunk.name, dir};

        if (!inputChunk.isClosed) {
          chain.push(chunk);
        } else if (chain.length === 0) {
          chunks.push(chunk);
        } else {
          chain.push(chunk);
          chunks.push({type: "transient", children: chain});
          chain = [];
        }
      }
      if (chain.length > 0) {
        chunks.push({type: "transient", children: chain});
        chain = [];
      }
    }
    const levelSetConfig: LevelSetConfig = {
      varName: inputLevelSetConfig.varName,
      did,
      hasPaddingLevel: inputLevelSetConfig.padding,
      ensureEqualLengths: inputLevelSetConfig.equalSize,
      chunks,
    };
    resolvedSets.set(key, levelSetConfig);
  }
  return {
    sets: resolvedSets,
    tags: dimConfig.tags,
    ways: dimConfig.ways,
  };
}
