import { Buffer } from "buffer";
import fs from "fs";
import { fromSysPath, toSysPath } from "furi";
import sysPath from "path";
import url from "url";

export async function outputFileAsync(filePath: url.URL, data: Buffer): Promise<void> {
  await mkdirpAsync(fromSysPath(sysPath.dirname(toSysPath(filePath.toString()))));
  await writeFileAsync(filePath, data);
}

export function readTextFileAsync(filePath: url.URL): Promise<string> {
  return new Promise<string>((resolve, reject): void => {
    fs.readFile(
      filePath,
      {encoding: "utf-8"},
      (err: NodeJS.ErrnoException | null, text: string): void => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(text);
        }
      },
    );
  });
}

export function mkdirpAsync(dirPath: url.URL): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.mkdir(dirPath, {recursive: true}, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export function writeFileAsync(filePath: url.URL, data: Buffer): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export function isDirectoryAsync(path: url.URL): Promise<boolean> {
  return new Promise<boolean>((resolve, _reject) => {
    fs.stat(path, (_err: NodeJS.ErrnoException | null, stats: fs.Stats) => {
      resolve(stats.isDirectory());
    });
  });
}
