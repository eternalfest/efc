// Global cache, for caching computed properties

export interface CacheKey<V> {
  readonly __ValueType?: V;
}

export class Cache {
  private readonly inner: WeakMap<object, any> = new WeakMap();

  constructor() {}

  public get<V, K extends CacheKey<V>>(key: K): V | undefined;
  public get<V, K extends CacheKey<V>>(key: K, compute: (k: K) => V): V;
  public get<V, K extends CacheKey<V>>(key: K, compute?: (k: K) => V): V | undefined {
    if (compute === undefined || this.inner.has(key)) {
      return this.inner.get(key);
    } else {
      const value: V = compute(key);
      this.inner.set(key, value);
      return value;
    }
  }

  public set<V, K extends CacheKey<V>>(key: K, value: V) {
    this.inner.set(key, value);
  }
}

export const CACHE: Cache = new Cache();
