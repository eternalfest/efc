import { evalExpr as bsEvalExpr } from "@eternalfest/better-scripts";
import { fromSysPath, toSysPath } from "furi";
import JSON5 from "json5";
import sysPath from "path";
import { firstValueFrom, Observable } from "rxjs";
import { mergeMap as rxMergeMap } from "rxjs/operators";
import stripJsonComments from "strip-json-comments";
import toml from "toml";
import { URL } from "url";

import { CompilerContext } from "./compiler/context.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  of as crOf,
} from "./compiler-result.mjs";
import * as diags from "./diagnostic/errors.mjs";
import { Diagnostic } from "./diagnostic/report.mjs";
import { observeNodes } from "./observable-fs.mjs";
import { withTextFile } from "./utils.mjs";

export type DataPrimitive = null | number | string | boolean;
export type DataValue = null | number | string | boolean | DataValue[] | { [P: string]: DataValue };

interface State {
  /**
   * Map from absolute paths to versions.
   */
  readonly nodes: ReadonlyMap<string, number>;

  /**
   * Map from keys to JSON values.
   */
  readonly data: ReadonlyMap<string, Cr<DataValue>>;
}

// TODO: use observables for CompilerContext
export function observe(cx: CompilerContext, dirUrl: URL): Observable<ReadonlyMap<string, Cr<DataValue>>> {
  let prev: State | undefined = undefined;

  return observeDataFiles(dirUrl).pipe(
    rxMergeMap(async (nodes: ReadonlyMap<string, number>): Promise<ReadonlyMap<string, Cr<DataValue>>> => {
      const state: State = await next(cx, nodes, prev);
      prev = state;
      return state.data;
    }),
  );
}

export async function get(cx: CompilerContext, dirUrl: URL): Promise<ReadonlyMap<string, Cr<DataValue>>> {
  const nodes$ = observeDataFiles(dirUrl);
  const nodes: ReadonlyMap<string, number> = await firstValueFrom(nodes$);
  return (await next(cx, nodes)).data;
}


function observeDataFiles(dirUrl: URL): Observable<ReadonlyMap<string, number>> {
  const dir: string = toSysPath(dirUrl);
  return observeNodes("*.{json,json5,toml}", {cwd: dir});
}

async function next(
  cx: CompilerContext,
  nodes: ReadonlyMap<string, number>,
  prev: State | undefined = undefined,
): Promise<State> {
  const data: Map<string, Cr<DataValue>> = new Map();

  // Ensure consistent ordering.
  const fsNodes: string[] = [...nodes.keys()];
  fsNodes.sort();

  for (const fsNode of fsNodes) {
    const extension: string = sysPath.extname(fsNode);
    const key: string = sysPath.basename(fsNode, extension);
    const url: URL = fromSysPath(fsNode);

    const existing: Cr<DataValue> | undefined = data.get(key);
    let value: Cr<DataValue>;
    if (existing !== undefined) {
      const err = diags.error(`Duplicate data file for key ${JSON.stringify(key)}`);
      err.fileUri = url;
      value = new Cr(existing.value, existing.diagnostics.concat(err));
    } else if (prev !== undefined && nodes.get(fsNode) === prev.nodes.get(fsNode) && prev.data.has(key)) {
      value = prev.data.get(key)!;
    } else {
      value = await withTextFile(url, text => parseDataValue(text, extension).flatMap(raw => {
        const diagList: Diagnostic[] = [];
        const data = processDataValue(cx, diagList, [], raw);
        return new Cr(data, diagList);
      }));
    }
    data.set(key, value);
  }

  return {nodes, data};
}

function processDataValue(cx: CompilerContext, diagList: Diagnostic[], path: (string | number)[], value: unknown): DataValue {
  switch (typeof value) {
    // Basic values.
    case "number":
    case "boolean":
      return value;

    // Strings starting with exactly one `#` are constant expressions that must be evaluated.
    case "string": {
      if (!value.startsWith("#")) {
        return value;
      } else if (value.startsWith("##")) {
        return value.substring(1);
      }

      const result = bsEvalExpr(value, cx.resolver);
      if (result.type === "error") {
        diagList.push({ ...diags.betterScriptError(result), sourceText: value, subfile: path.join(".") });
        return null;
      } else {
        return result.value;
      }
    }

    case "object": {
      if (value === null) {
        return null;
      } else if (value instanceof Date) {
        // TOML data files may contain dates, forbid them for consistency with JSON(5)
        const error = diags.error("Date literals aren't allowed in data files");
        error.subfile = path.join(".");
        diagList.push(error);
        return null;
      }

      let out: DataValue;
      const lastIdx: number = path.length;
      path.push(""); // dummy value.

      if (value instanceof Array) {
        // Recursively process arrays.
        out = new Array(value.length);
        for (let i = 0; i < value.length; i++) {
          path[lastIdx] = i;
          out[i] = processDataValue(cx, diagList, path, value[i]);
        }
      } else {
        // Recursively process records; note that record keys may be expressions too.
        out = {};
        const hasOwnProperty = Object.prototype.hasOwnProperty;
        for (const key in value) {
          path[lastIdx] = key;
          const realKey = String(processDataValue(cx, diagList, path, key));
          const realValue = processDataValue(cx, diagList, path, (value as any)[key]);

          if (hasOwnProperty.call(out, realKey)) {
            // Note that this only detects duplicate keys involving at least one constant expression,
            // e.g. `{ "#(2+2)": 4, "#(1+3)": 4 }`.
            //
            // 'True' duplicate keys are handled by the underlying format, with the following behavior:
            // - JSON and JSON5 accept them, overwriting the previous value;
            // - TOML rejects them with an error.
            const warn = diags.warn(`Duplicate key in data file: ${JSON.stringify(realKey)}`);
            warn.subfile = path.join(".");
            diagList.push(warn);
          }

          out[realKey] = realValue;
        }
      }

      path.pop();
      return out;
    }

    default:
      // This should never happen.
      throw new Error("UnexpectedDataType: " + typeof value);
  }
}

function parseDataValue(contents: string, extension: string): Cr<unknown> {
  switch (extension) {
    case ".json": {
      // We accept comments in JSON files, but they are deprecated.
      const stripped = stripJsonComments(contents);
      const diagList: Diagnostic[] = [];
      if (stripped !== contents) {
        diagList.push(diags.warn("Comments in .json data files are deprecated. Use the .json5 extension instead."));
      }

      try {
        return new Cr(JSON.parse(stripped), diagList);
      } catch (err: any) {
        return crError(diags.malformedJson(err));
      }
    }

    case ".json5":
      try {
        return crOf(JSON5.parse(contents));
      } catch (err: any) {
        return crError(diags.malformedJson5(err, contents));
      }

    case ".toml":
      try {
        return crOf(toml.parse(contents));
      } catch (err: any) {
        return crError(diags.malformedToml(err));
      }

    default:
      throw new Error(`UnexpectedDataExtension: ${extension}`);
  }
}
