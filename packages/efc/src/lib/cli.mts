import assert from "assert";
import { Buffer } from "buffer";
import { asFuri } from "furi";
import * as rx from "rxjs";
import { lastValueFrom } from "rxjs";
import {first as rxFirst,mergeMap as rxMergeMap} from "rxjs/operators";
import url from "url";
import urlJoin from "url-join";
import yargs, {Argv} from "yargs";

import { CompilerResult as Cr } from "./compiler-result.mjs";
import {
  createTtyReporter,
  DiagnosticCategory,
  DiagnosticReporter,
  DiagnosticReportSummary,
} from "./diagnostic/report.mjs";
import { outputFileAsync } from "./fs-utils.mjs";
import { Compiled, observeCompilationFromDirectory } from "./project.mjs";
import { unreachable } from "./utils.mjs";
import { VERSION } from "./version.mjs";

const DEFAULT_OUT_FILE: string = "game.xml";
const DEFAULT_WATCH: boolean = false;

export interface ResolvedConfig {
  readonly projectRoot: string;
  readonly outFile: string;
  readonly watch: boolean;
}

export type Action = MessageAction | RunAction;

export interface MessageAction {
  readonly action: "message";
  readonly message: string;
  readonly error?: Error;
}

export interface RunAction {
  readonly action: "run";
  readonly config: ResolvedConfig;
}

export type CliAction = MessageAction | CliRunAction;

export interface CliRunAction {
  readonly action: "run";
  readonly config: CliConfig;
}

export interface CliConfig {
  readonly projectRoot?: string;
  readonly outFile?: string;
  readonly watch?: boolean;
}

const ARG_PARSER: Argv = yargs([]);

ARG_PARSER
  .scriptName("efc")
  .version(VERSION)
  .usage("$0 [opts] [project]")
  .locale("en")
  .pkgConf("efc")
  .epilog("by Eternalfest")
  .option("out-file", {
    alias: "o",
    describe: "output file URL",
    default: DEFAULT_OUT_FILE,
  })
  .option("watch", {
    describe: "use watch mode",
    default: DEFAULT_WATCH,
    type: "boolean",
  });

/**
 * Executes the efc CLI
 *
 * @param args CLI arguments
 * @param cwd Current working directory
 * @param proc Current process
 */
export async function execCli(args: string[], cwd: string, proc: NodeJS.Process): Promise<number> {
  const action: Action = await getAction(args, cwd);

  switch (action.action) {
    case "message":
      proc.stderr.write(Buffer.from(`${action.message}\n`));
      return action.error === undefined ? 0 : 1;
    case "run":
      return execRunAction(action.config, proc);
    default:
      return unreachable(action, "Unexpected Action.action");
  }
}

export async function getAction(args: string[], cwd: string): Promise<Action> {
  const parsed: CliAction = parseArgs(args);
  if (parsed.action !== "run") {
    return parsed;
  }
  return {
    action: "run",
    config: resolveConfig(cwd, parsed.config),
  };
}

export function parseArgs(args: string[]): CliAction {
  // The yargs pure API is kinda strange to use (apart from requiring a callback):
  // The error can either be defined, `undefined` or `null`.
  // If it is defined or `null`, then `output` should be a non-empty string
  // intended to be written to stderr. `parsed` is defined but it should be
  // ignored in this case.
  // If `err` is `undefined`, then `output` is an empty string and `parsed`
  // contains the succesfully parsed args.
  // tslint:disable:variable-name
  let _err: Error | undefined | null;
  let _parsed: any;
  let _output: string;
  let isParsed: boolean = false;
  ARG_PARSER.parse(args, (err: Error | undefined | null, parsed: any, output: string): void => {
    _err = err;
    _parsed = parsed;
    _output = output;
    isParsed = true;
  });
  assert(isParsed);
  const err: Error | undefined | null = _err!;
  const parsed: any = _parsed!;
  const output: string = _output!;
  if (err === null) {
    // Successfully parsed
    return {
      action: "run",
      config: {
        outFile: parsed.DEFAULT_OUT_FILE,
        // projectRoot: parsed._.length > 0 ? parsed[0] : undefined,
        watch: parsed.watch,
      },
    };
  } else {
    return {action: "message", message: output, error: err};
  }
}

function resolveConfig(cwd: string, cliConfig: CliConfig): ResolvedConfig {
  const outFile: string = cliConfig.outFile !== undefined ? cliConfig.outFile : DEFAULT_OUT_FILE;
  const watch: boolean = cliConfig.watch !== undefined ? cliConfig.watch : DEFAULT_WATCH;

  return {
    projectRoot: cwd,
    outFile: urlJoin(cwd, outFile),
    watch,
  };
}

async function execRunAction(config: ResolvedConfig, proc: NodeJS.Process): Promise<number> {
  if (config.watch) {
    proc.stderr.write(Buffer.from("Using watch mode\n"));
  }
  const outData$: rx.Observable<Cr<Compiled>> = observeCompilationFromDirectory(new url.URL(config.projectRoot));

  let exitCode$: rx.Observable<number> = outData$
    .pipe(rxMergeMap(outData => rx.from(onOutData(outData))));

  if (!config.watch) {
    exitCode$ = exitCode$.pipe(rxFirst());
  }
  return lastValueFrom(exitCode$);

  async function onOutData(outData: Cr<Compiled>): Promise<number> {
    try {
      const reporter: DiagnosticReporter = createTtyReporter(proc.stderr);
      const summary: DiagnosticReportSummary = reporter(outData.diagnostics);
      const numErrors: number = summary.byCategory.get(DiagnosticCategory.Error)!;
      const numWarnings: number = summary.byCategory.get(DiagnosticCategory.Warning)!;

      if (numErrors > 0) {
        proc.stderr.write(Buffer.from(
          `Compilation failed with ${numErrors} error(s) and ${numWarnings} warning(s).\n`,
        ));
        return 1;
      } else {
        const data: Compiled = outData.unwrap();
        await outputFileAsync(asFuri(config.outFile), Buffer.from(data.content));
        if (numWarnings > 0) {
          proc.stderr.write(Buffer.from(`Compilation finished with ${numWarnings} warning(s).\n`));
        } else {
          proc.stderr.write(Buffer.from("Compilation finished.\n"));
        }
        return 0;
      }
    } catch (err) {
      if (err instanceof Error) {
        proc.stderr.write(Buffer.from(`InternalCompilerError:\n${err.stack}\n`));
      } else {
        proc.stderr.write(Buffer.from(`InternalCompilerError:\n${err}\n`));
      }
      return 1;
    }
  }
}
