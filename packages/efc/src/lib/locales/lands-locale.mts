import { LevelRefMap } from "../compiler/links.mjs";
import { CompilerResult as Cr, errors as crErrors, of as crOf } from "../compiler-result.mjs";
import { Diagnostic } from "../diagnostic/report.mjs";
import { JavaDimensionsLocale } from "../java/dimensions-locale.mjs";

export interface LandsLocale {
  getNames(): ReadonlyMap<string, ReadonlyMap<number, string>>;

  // getName(ref: LevelRef): string | undefined;
}

export function fromJavaDimensions(javaLocales: JavaDimensionsLocale, levelRefMap: LevelRefMap): Cr<LandsLocale> {
  const diags: Diagnostic[] = [];
  const names: Map<string, Map<number, string>> = new Map();
  for (const [levelRef, name] of javaLocales) {
    const cr: Cr<void> = levelRefMap.resolve(levelRef).map(resolved => {
      let levelSet: Map<number, string> | undefined = names.get(resolved.did);
      if (levelSet === undefined) {
        levelSet = new Map();
        names.set(resolved.did, levelSet);
      }
      levelSet.set(resolved.levelId, name);
    });

    for (const diag of cr.diagnostics) {
      diags.push(diag);
    }

  }
  return diags.length === 0 ? crOf(new DefaultLandsLocale(names)) : crErrors(diags);
}

class DefaultLandsLocale implements LandsLocale {
  names: ReadonlyMap<string, ReadonlyMap<number, string>>;

  constructor(names: ReadonlyMap<string, ReadonlyMap<number, string>>) {
    this.names = names;
  }

  // getName(ref: LevelRef): string | undefined {
  //   const levelSet: ReadonlyMap<string, string> | undefined = this.names.get(ref.setId);
  //   if (levelSet === undefined) {
  //     return undefined;
  //   }
  //   return levelSet.get(ref.levelId.toString(10));
  // }

  getNames(): ReadonlyMap<string, ReadonlyMap<number, string>> {
    return this.names;
  }
}
