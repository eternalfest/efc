import { Cheerio } from "cheerio";
import { Element } from "domhandler";

import { LandsLocale } from "./lands-locale.mjs";

export interface Locale {
  readonly statics: Cheerio<Element>;
  readonly items: Cheerio<Element>;
  readonly lands: LandsLocale;
  readonly keys: Cheerio<Element>;
  readonly quests: undefined;
  readonly families: undefined;
}
