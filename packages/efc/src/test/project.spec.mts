import * as assert from "node:assert/strict";
import {describe, test} from "node:test";

import { fromSysPath as furiFromSyspath } from "furi";

import { CompilerResult as Cr } from "../lib/compiler-result.mjs";
import {
  Diagnostic,
  TextReporter
} from "../lib/diagnostic/report.mjs";
import { Compiled, compileFromDirectory } from "../lib/project.mjs";
import { getSampleProjectsErrSync, getSampleProjectsSync, REPO_ROOT } from "./samples/samples.mjs";

describe("project", function () {
  describe("compileFromDirectory (Ok)", function () {
    for (const sample of getSampleProjectsSync()) {
      test(sample.name, async () => {
        const actualCompiled: Cr<Compiled> = await compileFromDirectory(sample.dir, true);
        const actual: Cr<string> = actualCompiled.map(x => x.content);
        const reporter = new TextReporter();
        reporter.report(actual.diagnostics);
        assert.strictEqual(remapRepoRoot(reporter.getOutput()), "");
        assert.deepStrictEqual(actual.value, sample.expected);
      });
    }
  });

  describe("compileFromDirectory (Err)", function () {
    for (const sample of getSampleProjectsErrSync()) {
      test(sample.name, async () => {
        const actualCompiled: Cr<Compiled> = await compileFromDirectory(sample.dir, true);
        const actualDiagnostics: readonly Diagnostic[] = actualCompiled.diagnostics;
        const reporter = new TextReporter();
        const actualSummary = reporter.report(actualDiagnostics);
        assert.ok(actualSummary !== null && typeof actualSummary === "object");
        const actualReport: string = remapRepoRoot(reporter.getOutput());
        const expectedReport: string = sample.expected;
        assert.strictEqual(actualReport, expectedReport);
      });
    }
  });
});

function remapRepoRoot(report: string): string {
  const repoRootUri: string = furiFromSyspath(REPO_ROOT).toString();
  return report
    .replaceAll(`${repoRootUri}/`, "file://$REPO_ROOT/")
    .replaceAll(`${REPO_ROOT}/`, "$REPO_ROOT/");
}
