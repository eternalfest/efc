import * as assert from "node:assert/strict";
import {describe, test} from "node:test";

import * as mtbon from "@eternalfest/mtbon";

import { CompilerContext, createCompilerContext } from "../lib/compiler/context.mjs";
import { javaToHfLevel } from "../lib/compiler/convert.mjs";
import * as hf from "../lib/compiler/hf.mjs";
import { identityObf } from "../lib/compiler/obf.mjs";
import { CompilerResult as Cr, of as crOf } from "../lib/compiler-result.mjs";
import { DiagnosticCategory } from "../lib/diagnostic/report.mjs";
import { BASE_ASSETS_DIR, fromDirectories, JavaAssets } from "../lib/java/assets.mjs";
import * as java from "../lib/java/level.mjs";
import { getSampleLevelsSync } from "./samples/samples.mjs";

function filterWarnings<T>(cr: Cr<T>): Cr<T> {
  return cr.mapDiagnostics(diag => diag.category === DiagnosticCategory.Error ? diag : undefined);
}

describe("convert", async () => {

  const assets: JavaAssets = (await fromDirectories(BASE_ASSETS_DIR)).unwrap();

  describe("samples", () => {
    for (const sample of getSampleLevelsSync()) {
      test(sample.name, async () => {
        const expectedHfLevel: Cr<hf.Level> = crOf(sample.hfl);
        const inputLevel: Cr<java.JavaLevel> = java.fromString(sample.lvl);
        const actualHfLevel: Cr<hf.Level> = inputLevel.flatMap(lvl => {
          const cx: CompilerContext = createCompilerContext(assets, identityObf());
          return javaToHfLevel(cx, lvl);
        });
        assert.deepStrictEqual(filterWarnings(actualHfLevel), expectedHfLevel);

        const expectedMtbon: Cr<string> = crOf(sample.mtbon.trim());
        const actualMtbon: Cr<string> = actualHfLevel.map(hfLevel => {
          const escapedHfl: hf.EscapedLevel = hf.escapeLevel(hfLevel);
          return mtbon.emit(escapedHfl);
        });
        assert.deepStrictEqual(filterWarnings(actualMtbon), expectedMtbon);
      });
    }
  });
});
