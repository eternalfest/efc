import * as assert from "node:assert/strict";
import {describe, test} from "node:test";

import { fromSysPath } from "furi";
import { firstValueFrom } from "rxjs";

import { CompilerResult as Cr } from "../lib/compiler-result.mjs";
import { TextReporter } from "../lib/diagnostic/report.mjs";
import { fromConfig as javaAssetsFromConfig, JavaAssetRef, JavaAssets } from "../lib/java/assets.mjs";
import {
  EfcAssetsConfig,
  fromPath as javaConfigFromPath,
  ResolvedConfig,
} from "../lib/java/config.mjs";
import { getSampleAssetsConfigsSync } from "./samples/samples.mjs";

function mapToRecord(map: ReadonlyMap<string, JavaAssetRef>): Record<string, JavaAssetRef> {
  const obj: Record<string, JavaAssetRef> = {};
  for (const [key, value] of map) {
    obj[key] = { ...value };
  }
  return obj;
}

function javaAssetsToRawObject(javaAssets: JavaAssets): any {
  return {
    backgrounds: mapToRecord(javaAssets.backgrounds),
    fields: mapToRecord(javaAssets.fields),
    tiles: mapToRecord(javaAssets.tiles),
    bads: mapToRecord(javaAssets.bads),
  };
}

describe("assets", () => {
  describe("samples", function () {
    for (const sample of getSampleAssetsConfigsSync()) {
      test(sample.name, async () => {
        const config: Cr<ResolvedConfig> = await firstValueFrom(javaConfigFromPath(sample.config));
        const assets: Cr<JavaAssets> = await config.flatMapAsync(async config => {
          const efcAssetsConfig: EfcAssetsConfig = {
            useBaseAssets: config.assets.useBaseAssets,
            roots: config.assets.roots.map(fromSysPath),
          };
          return javaAssetsFromConfig(efcAssetsConfig);
        });

        const actualAssets: Cr<any> = assets.map(javaAssetsToRawObject);

        const reporter: TextReporter = new TextReporter();
        reporter.report(actualAssets.diagnostics);
        assert.strictEqual(reporter.getOutput(), "");

        assert.deepStrictEqual(actualAssets.value, sample.expected);
      });
    }
  });
});
