import { Buffer } from "buffer";
import fs from "fs";
import { fromSysPath } from "furi";
import path from "path";
import url from "url";

import * as hf from "../../lib/compiler/hf.mjs";

export const REPO_ROOT: string = path.resolve(url.fileURLToPath(import.meta.url), "../../..");

export const TEST_DIR: string = path.resolve(REPO_ROOT, "test-resources");
export const LEVELS_DIR: string = path.resolve(TEST_DIR, "levels");
export const LEVEL_SETS_DIR: string = path.resolve(TEST_DIR, "level-sets");
export const PROJECTS_DIR: string = path.resolve(TEST_DIR, "projects");
export const PROJECTS_ERR_DIR: string = path.resolve(TEST_DIR, "projects-err");
export const ASSETS_CONFIGS_DIR: string = path.resolve(TEST_DIR, "assets");
export const SAMPLE_DATA_FILES_DIR: string = path.resolve(TEST_DIR, "data-files");

export interface SampleLevel {
  readonly name: string;
  readonly hfl: hf.Level;
  readonly lvl: string;
  readonly mtbon: string;
}

export interface SampleLevelSet {
  readonly name: string;
  readonly dirPath: string;
  readonly mtbon: string;
}

export interface SampleProject {
  readonly name: string;
  readonly dir: url.URL;
  readonly expected: string;
}

export interface SampleAssets {
  readonly name: string;
  readonly config: url.URL;
  readonly expected: Readonly<any>;
}

export interface SampleDataFiles {
  readonly dir: url.URL;
  readonly expected: Readonly<any>;
  readonly report: string;
}

const SAMPLE_LEVELS: ReadonlyArray<string> = [
  "eternoel-001",
  "hf0",
  "hf1",
  "hf2",
  "hf6",
  "minimal",
  "minimal-no-script",
  "script-const-eval",
];

export function* getSampleLevelsSync(): Iterable<SampleLevel> {
  for (const sample of SAMPLE_LEVELS) {
    const mtbonPath: string = path.join(LEVELS_DIR, `${sample}.mtbon`);
    const hflPath: string = path.join(LEVELS_DIR, `${sample}.hfl`);
    const lvlPath: string = path.join(LEVELS_DIR, `${sample}.lvl`);

    yield {
      name: sample,
      mtbon: readTextSync(mtbonPath),
      hfl: readJsonSync(hflPath),
      lvl: readTextSync(lvlPath),
    };
  }
}

const SAMPLE_LEVEL_SETS: ReadonlyArray<string> = [
  "adv_puits",
];

export function* getSampleLevelSetsSync(): Iterable<SampleLevelSet> {
  for (const sample of SAMPLE_LEVEL_SETS) {
    const dirPath: string = path.join(LEVEL_SETS_DIR, sample);
    const mtbonPath: string = path.join(LEVEL_SETS_DIR, `${sample}.mtbon`);

    yield {
      name: sample,
      dirPath,
      mtbon: readTextSync(mtbonPath),
    };
  }
}

const SAMPLE_PROJECTS: ReadonlyArray<string> = [
  "sous-la-colline",
  "eternoel",
  "grid",
  "grid-ncp",
  "grid-unsorted",
  "timeattack",
];

export function* getSampleProjectsSync(): Iterable<SampleProject> {
  for (const sample of SAMPLE_PROJECTS) {
    const dirPath: string = path.join(PROJECTS_DIR, sample);
    const expectedPath: string = path.join(PROJECTS_DIR, `${sample}.xml`);

    yield {
      name: sample,
      dir: fromSysPath(dirPath),
      expected: readTextSync(expectedPath),
    };
  }
}

const SAMPLE_PROJECTS_ERR: ReadonlyArray<string> = [
  "empty",
];

export function* getSampleProjectsErrSync(): Iterable<SampleProject> {
  for (const sample of SAMPLE_PROJECTS_ERR) {
    const dirPath: string = path.join(PROJECTS_ERR_DIR, sample);
    const expectedPath: string = path.join(PROJECTS_ERR_DIR, `${sample}.txt`);

    yield {
      name: sample,
      dir: fromSysPath(dirPath),
      expected: readTextSync(expectedPath),
    };
  }
}

export function getSampleDataFilesSync(): SampleDataFiles {
  const expectedPath: string = path.join(SAMPLE_DATA_FILES_DIR, "expected", "data.json");
  const reportPath: string = path.join(SAMPLE_DATA_FILES_DIR, "expected", "report.txt");
  return {
    dir: fromSysPath(SAMPLE_DATA_FILES_DIR),
    expected: readJsonSync(expectedPath),
    report: readTextSync(reportPath),
  };
}

const SAMPLE_ASSETS_CONFIGS: ReadonlyArray<string> = [
  "minibase",
  "minibase-and-extra",
  "aliases",
];

export function* getSampleAssetsConfigsSync(): Iterable<SampleAssets> {
  for (const sample of SAMPLE_ASSETS_CONFIGS) {
    const configPath: string = path.join(ASSETS_CONFIGS_DIR, `${sample}.xml`);
    const expectedPath: string = path.join(ASSETS_CONFIGS_DIR, `${sample}.json`);
    const expected: any = readJsonSync(expectedPath);

    yield {
      name: sample,
      config: fromSysPath(configPath),
      expected,
    };
  }
}

function readTextSync(p: string): string {
  return fs.readFileSync(p, {encoding: "utf-8"});
}

function readJsonSync(p: string): any {
  const buffer: Buffer = fs.readFileSync(p);
  return JSON.parse(buffer.toString("utf-8"));
}
