import * as assert from "node:assert/strict";
import { describe, test } from "node:test";

import { fromSysPath } from "furi";

import { CompilerContext, createCompilerContext } from "../lib/compiler/context.mjs";
import { identityObf } from "../lib/compiler/obf.mjs";
import { hasValue as crHasValue } from "../lib/compiler-result.mjs";
import { DataValue, get as getDataFiles } from "../lib/data-files.mjs";
import { Diagnostic, TextReporter } from "../lib/diagnostic/report.mjs";
import { BASE_ASSETS_DIR, fromDirectories, JavaAssets } from "../lib/java/assets.mjs";
import { getSampleDataFilesSync, REPO_ROOT } from "./samples/samples.mjs";

describe("data files", async () => {
  const assets: JavaAssets = (await fromDirectories(BASE_ASSETS_DIR)).unwrap();

  test("sample", async () => {
    const cx: CompilerContext = createCompilerContext(assets, identityObf());
    const expected = getSampleDataFilesSync();
    const compiled: Record<string, DataValue> = {};
    const diagnostics: Diagnostic[] = [];
    for (const [k, v] of await getDataFiles(cx, expected.dir)) {
      if (crHasValue(v)) {
        compiled[k] = v.value;
      }
      for (const d of v.diagnostics) {
        diagnostics.push(d);
      }
    }

    const reporter = new TextReporter();
    reporter.report(diagnostics);
    const actualReport: string = remapRepoRoot(reporter.getOutput());

    assert.strictEqual(actualReport, expected.report);
    assert.deepStrictEqual(compiled, expected.expected);
  });
});

function remapRepoRoot(report: string): string {
  const repoRootUri: string = fromSysPath(REPO_ROOT).toString();
  return report
    .replaceAll(`${repoRootUri}/`, "file://$REPO_ROOT/")
    .replaceAll(`${REPO_ROOT}/`, "$REPO_ROOT/");
}
