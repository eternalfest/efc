#!/usr/bin/env node
import { fromSysPath } from "furi";
import process from "process";

import { execCli } from "../lib/cli.mjs";

export async function main(): Promise<void | never> {
  const args: string[] = process.argv.slice(2);
  const cwd: string = fromSysPath(process.cwd()).toString();
  const returnCode: number = await execCli(args, cwd, process);
  if (returnCode !== 0) {
    process.exit(returnCode);
  }
}

main();
